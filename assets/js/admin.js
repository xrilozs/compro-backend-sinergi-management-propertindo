let ADMIN_ID
$(document).ready(function(){
  //data table
  let admin_table = $('#admin-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${ADMIN_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "fullname",
            orderable: false
          },
          { 
            data: "username",
            orderable: false
          },
          {
            data: "is_active",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'primary' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-success admin-update-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>`
              if(parseInt(row.is_active)){
                if(row.role != 'SUPERADMIN'){
                  button += ` <button class="btn btn-sm btn-danger admin-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-inactive-modal" title="nonaktifkan">
                    <i class="fas fa-times"></i>
                  </button>`
                }
              }else{
                  button += ` <button class="btn btn-sm btn-info admin-active-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-active-modal" title="aktifkan">
                    <i class="fas fa-check"></i>
                  </button>`
              }
              
              return button
            },
            orderable: false
          }
      ]
  });
  
  //toggle
  $('#admin-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".admin-update-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
    $('#admin-update-overlay').show()

    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}/id/${ADMIN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#admin-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#admin-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".admin-inactive-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
  })
  
  $("body").delegate(".admin-active-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
  })

  //form
  $('#admin-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#admin-create-button')
    
    let $form = $(this),
        request = {
          fullname: $form.find( "input[name='fullname']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
          password: $form.find( "input[name='password']" ).val(),
        }
        
    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-create-button', 'Simpan')
          $('#admin-create-modal').modal('hide')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#admin-update-button')

    let $form = $(this),
        request = {
          id: ADMIN_ID,
          fullname: $form.find( "input[name='fullname']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
        }

    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-update-button', 'Simpan')
          $('#admin-update-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-inactive-button').click(function (){
    startLoadingButton('#admin-inactive-button')
    
    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}/inactive/${ADMIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-inactive-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-inactive-button', 'Simpan')
          $('#admin-inactive-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-active-button').click(function (){
    startLoadingButton('#admin-active-button')
    
    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}/active/${ADMIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-active-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-active-button', 'Simpan')
          $('#admin-active-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#admin-${type}-form`)
  $form.find( "input[name='fullname']" ).val(data.fullname)
  $form.find( "input[name='username']" ).val(data.username)

  let color = data.is_active ? 'success' : 'danger'
  let text = data.is_active ? 'AKTIF' : 'NONAKTIF'
  let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`
  $(`#admin-status-${type}-field`).html(badge)
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#admin-${type}-form`)
  $form.find( "input[name='fullname']" ).val("")
  $form.find( "input[name='username']" ).val("")
  if(type=='create') $form.find( "input[name='password']" ).val("")
}