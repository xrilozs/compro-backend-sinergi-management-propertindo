$(document).ready(function(){
  getActiveProductTotal()
  getActiveServiceTotal()
  getActivePortofolioTotal()
  getCategoryTotal()
  getUnreadMessageTotal()
});

function getActiveProductTotal(){
  $.ajax({
    async: true,
    url: `${PRODUCT_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-product-total').html(response)
    }
  });
}

function getActiveServiceTotal(){
  $.ajax({
    async: true,
    url: `${SERVICE_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("RES: ", res.responseText)
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-service-total').html(response)
    }
  });
}

function getActivePortofolioTotal(){
  $.ajax({
    async: true,
    url: `${PORTOFOLIO_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-portofolio-total').html(response)
    }
  });
}

function getCategoryTotal(){
  $.ajax({
    async: true,
    url: `${PRODUCT_CATEGORY_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-category-total').html(response)
    }
  });
}

function getUnreadMessageTotal(){
  $.ajax({
    async: true,
    url: `${MESSAGE_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-message-total').html(response)
    }
  });
}