let SUB_SERVICE_ID,
    SERVICE_ID,
    service_table

$(document).ready(function(){
  $('#subservice-create-image').dropify()
  getService()

  //render datatable
  service_table = $('#subservice-datatable').DataTable( {
      paging: false,      // Disable pagination
      info: false,        // Disable information display
      searching: false,   // Disable search box
      ordering: false,    // Disable sorting
      bInfo: false,       // Disable bottom information (used in older versions)
      bPaginate: false,   // Disable pagination (used in older versions)
      bLengthChange: false,  // Disable the ability to change page length (used in older versions)
      bFilter: false,     // Disable the search/filtering feature (used in older versions)
      bSort: false,       // Disable sorting (used in older versions)
      bAutoWidth: false,
      columns: [
          { 
            data: "img_url",
            render: function (data, type, row, meta) {
              return `<img src="${WEB_URL+"/"+data}" class="img-responsive" style="width:200px;">`
            },
            orderable: false
          },
          { 
            data: "service",
            orderable: false
          },
          { 
            data: "name",
            orderable: false
          },
          { 
            data: "description",
            orderable: false,
            render: function (data, type, row, meta) {
              return sortText(data, 50)
            }
          },
          { 
            data: "is_active",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'success' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-${color}">${text}</span>`

              return badge
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let isActive = parseInt(row.is_active)
              let button = `<button class="btn btn-sm btn-success subservice-update-toggle" data-id="${data}" data-toggle="modal" data-target="#subservice-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-${getActionColor(isActive)} subservice-${getAction(isActive)}-toggle" data-id="${data}" data-toggle="modal" data-target="#subservice-${getAction(isActive)}-modal" title="${getAction(isActive)}">
                <i class="fas fa-${getActionIcon(isActive)}"></i>
              </button>`

              if(!isActive){
                button += `<button class="btn btn-sm btn-danger subservice-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#subservice-delete-modal" title="hapus sub servis">
                  <i class="fas fa-trash"></i>
                </button>`
              }
              return button
            },
            orderable: false
          }
      ]
  })

  $('#service-filter').change(function(){
    SERVICE_ID = $(this).val()
    if(SERVICE_ID){
      getSubService()
    }else{
      service_table.clear().draw()
    }
  })

  $('#subservice-create-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "create")
  });

  $('#subservice-update-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "update")
  });
  
  //button action click
  $("#subservice-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".subservice-update-toggle", "click", function(e) {
    SUB_SERVICE_ID = $(this).data('id')
    clearForm('update')

    $('#subservice-update-overlay').show()
    $.ajax({
        async: true,
        url: `${SUB_SERVICE_API_URL}/id/${SUB_SERVICE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#subservice-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#subservice-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".subservice-delete-toggle", "click", function(e) {
    SUB_SERVICE_ID = $(this).data('id')
    console.log("ID:", SUB_SERVICE_ID)
  })

  $("body").delegate(".subservice-inactive-toggle", "click", function(e) {
    SUB_SERVICE_ID = $(this).data('id')
    console.log("ID:", SUB_SERVICE_ID)
  })

  $("body").delegate(".subservice-active-toggle", "click", function(e) {
    SUB_SERVICE_ID = $(this).data('id')
    console.log("ID:", SUB_SERVICE_ID)
  })

  //submit form
  $('#subservice-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#subservice-create-button')
    
    let $form = $(this)
    let request = {
      name: $form.find( "input[name='name']" ).val(),
      description: $form.find( "textarea[name='description']" ).val(),
      service_id: $("#subservice-service-create-field").find(":selected").val(),
      img_url: IMG_URL
    }
    console.log
    $.ajax({
        async: true,
        url:SUB_SERVICE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#subservice-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#subservice-create-button', 'Simpan')
          showSuccess(res.message)
          $('#subservice-create-modal').modal('hide')
          if(SERVICE_ID){
            getSubService()
          }
        }
    });
  })
  
  $('#subservice-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#subservice-update-button')

    let $form = $(this)
    let data = {
      id: SUB_SERVICE_ID,
      service_id: $("#subservice-service-update-field").find(":selected").val(), 
      name: $form.find( "input[name='name']" ).val(),
      description: $form.find( "textarea[name='description']" ).val(),
      img_url: IMG_URL
    }

    $.ajax({
        async: true,
        url:SUB_SERVICE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#subservice-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#subservice-update-button', 'Simpan')
          showSuccess(res.message)
          $('#subservice-update-modal').modal('toggle')
          if(SERVICE_ID){
            getSubService()
          }
        }
    });
  })
  
  $('#subservice-delete-button').click(function (){
    startLoadingButton('#subservice-delete-button')
    
    $.ajax({
        async: true,
        url: `${SUB_SERVICE_API_URL}/delete/${SUB_SERVICE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#subservice-delete-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#subservice-delete-button', 'Ya')
          showSuccess(res.message)
          $('#subservice-delete-modal').modal('toggle')
          if(SERVICE_ID){
            getSubService()
          }
        }
    });
  })

  $('#subservice-inactive-button').click(function (){
    startLoadingButton('#subservice-inactive-button')
    
    $.ajax({
        async: true,
        url: `${SUB_SERVICE_API_URL}/inactive/${SUB_SERVICE_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#subservice-inactive-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#subservice-inactive-button', 'Ya')
          showSuccess(res.message)
          $('#subservice-inactive-modal').modal('toggle')
          if(SERVICE_ID){
            getSubService()
          }
        }
    });
  })

  $('#subservice-active-button').click(function (){
    startLoadingButton('#subservice-active-button')
    
    $.ajax({
        async: true,
        url: `${SUB_SERVICE_API_URL}/active/${SUB_SERVICE_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#subservice-active-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#subservice-active-button', 'Ya')
          showSuccess(res.message)
          $('#subservice-active-modal').modal('toggle')
          if(SERVICE_ID){
            getSubService()
          }
        }
    });
  })
})

function getSubService(){
  $.ajax({
    async: true,
    url: `${SUB_SERVICE_API_URL}/service/${SERVICE_ID}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      service_table.clear().rows.add(response).draw();
    }
  });
}

function getService(){
  $.ajax({
    async: true,
    url: `${SERVICE_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      renderServiceOption(response)
    }
  });
}

function upload_image(file, type){
  $(`#subservice-${type}-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${SUB_SERVICE_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#subservice-${type}-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        IMG_URL = response.file_url
        $(`#subservice-${type}-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderServiceOption(data){
  let service_html = "<option>-Pilih Servis-</option>"
  data.forEach(item => {
    let option_html = `<option value="${item.id}">${item.name}</option>`
    service_html += option_html
  });
  $(`.service-option`).html(service_html)
}

function renderForm(data, type){
  let $form = $(`#subservice-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "textarea[name='description']" ).val(data.description)
  $(`#subservice-service-${type}-field`).val(data.service_id).change()
  if(type == 'update'){
    // $('#subservice-update-image').attr("data-default-file", `${WEB_URL}/${data.img_url}`);
    // $('#subservice-update-image').dropify()
    var drEvent = $('#subservice-update-image').dropify({
        defaultFile: `${WEB_URL}/${data.img_url}`
    });
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    drEvent.settings.defaultFile = `${WEB_URL}/${data.img_url}`;
    drEvent.destroy();
    drEvent.init();

    IMG_URL = data.img_url
  }
}

function clearForm(type){
  let $form = $(`#subservice-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $form.find( "textarea[name='description']" ).val("")
}

function getAction(isActive){
  return parseInt(isActive) ? 'inactive' : 'active'
}

function getActionIcon(isActive){
  return parseInt(isActive) ? 'times' : 'check'
}

function getActionColor(isActive){
  return parseInt(isActive) ? 'warning' : 'info'
}
