let ADMIN_API_URL             = `${API_URL}/admin`
let BANNER_API_URL            = `${API_URL}/banner`
let CONFIG_API_URL            = `${API_URL}/config`
let PAGE_API_URL              = `${API_URL}/custom-page`
let FAQ_API_URL               = `${API_URL}/faq`
let MESSAGE_API_URL           = `${API_URL}/message`
let PORTOFOLIO_API_URL        = `${API_URL}/portofolio`
let PORTOFOLIO_IMAGE_API_URL  = `${API_URL}/portofolio/image`
let PRODUCT_API_URL           = `${API_URL}/product`
let PRODUCT_IMAGE_API_URL     = `${API_URL}/product/image`
let PRODUCT_CATEGORY_API_URL  = `${API_URL}/product-category`
let SERVICE_API_URL           = `${API_URL}/service`
let SUB_SERVICE_API_URL       = `${API_URL}/sub-service`
let SESSION                   = localStorage.getItem("user-token")
let REFRESH_SESSION           = localStorage.getItem("user-refresh-token")
let ADMIN_FULLNAME            = localStorage.getItem("user-fullname")
let RETRY_COUNT               = 0

let TOAST = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

$(".datepicker").datepicker( {
  format: "yyyy-mm-dd",
  autoclose: true
});

$('.rich-text-create').summernote({
  placeholder: 'Type here..',
  height: 300
});

$('.rich-text-update').summernote({
  placeholder: 'Type here..',
  height: 300
});

$('.select2').select2()

$('.dropify').dropify();

$(document).ready(function(){
  if(!SESSION){
    window.location.href = `${WEB_URL}/login`
  }

  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      lengthMenu: "Tampil _MENU_ Data",
      zeroRecords: 'Tidak ada data',
      emptyTable: 'Data tidak ditemukan',
      search: "Cari",
      info: 'Total data _TOTAL_',
      processing: 'Memproses..',
      paginate: {
        first: 'Pertama',
        previous: 'Sebelumnya',
        next: 'Selanjutnya',
        last: 'Terakhir'
      },
      // Add other language options as needed
    }
  });
  
  renderHeader()
  getConfig()
});

function renderHeader(){
  $('#header-admin-name').html(`${ADMIN_FULLNAME}`)
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  const admin = data.admin
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  localStorage.setItem("user-fullname", admin.fullname);
  SESSION         = data.access_token
  REFRESH_SESSION = data.refresh_token
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-fullname");
  window.location.href = `${WEB_URL}/login`
}

function refreshToken(){
  let resp = {}
  $.ajax({
      async: false,
      url: `${API_URL}/admin/refresh`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${REFRESH_SESSION}`);
      },
      error: function(res) {
        resp = {status: "failed"}
      },
      success: function(res) {
        const response = res.data
        resp = {status: "success", data: response}
      }
  });
  
  return resp
}

function retryRequest(responseError){
  console.log("RETRY: ", RETRY_COUNT)
  if(responseError.code == 401){
    if(RETRY_COUNT < 3){
      let resObj = refreshToken()          
      if(resObj.status == 'success'){
        RETRY_COUNT += 1 
        setSession(resObj.data)
        return true
      }else if(resObj.status == 'failed'){
        removeSession()
      }
    }else{
      removeSession()
    }
  }else{
    showError(responseError.message)
    return false
  }
}

function formatRupiah(angka, prefix){
  var angkaStr  = angka.replace(/[^,\d]/g, '').toString(),
      split     = angkaStr.split(','),
      sisa      = split[0].length % 3,
      rupiah    = split[0].substr(0, sisa),
      ribuan    = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah    += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function formatDateReverse(datetimeStr){
  let datetimeArr = datetimeStr.split(" ")
  let dateStr     = datetimeArr[0]
  let arr         = dateStr.split("-")
  return `${arr[2]}-${arr[1]}-${arr[0]}`
}

function sortText(text, length=20, isRemoveHTMLTag=false){
  if(isRemoveHTMLTag){
    text = text.replace(/<[^>]+>/g, '');
    console.log("text: ", text)
  }
  return text.slice(0, length) + ".."
}

$(document).on('keydown', '.input-decimal', function(e){
  console.log("input decimal")
  let input   = $(this);
  let oldVal  = input.val();
  let regex
  let attr    = input.attr('pattern')
  if (typeof attr !== 'undefined' && attr !== false) {
    regex = new RegExp(input.attr('pattern'), 'g');
  }else{
    regex = new RegExp(/^\s*-?(\d+(\.\d{1,2})?|\.\d{1,2})\s*$/, 'g')
  }
  
  setTimeout(function(){
    var newVal = input.val();
    if(!regex.test(newVal)){
      input.val(oldVal); 
    }
  }, 1);
});

$(document).on('change', '.input-currency', function(e){
  var oldVal = $(this).val();
  $(this).val(formatRupiah(oldVal))
});

$("#logout-button").click(function(){    
  removeSession()
})

$("#change-password-toggle").click(function(e){
  let $form = $("#change-password-form" )
  $form.find( "input[name='newPassword']" ).val('')
  $form.find( "input[name='confirmPassword']" ).val('')
})

$("#change-password-form").submit(function(e){
  e.preventDefault()
  startLoadingButton("#change-password-button")

  let $form = $( this ),
      password        = $form.find( "input[name='newPassword']" ).val(),
      confirmPassword = $form.find( "input[name='confirmPassword']" ).val()

  if(password != confirmPassword){
    endLoadingButton('#change-password-button', 'Simpan')
    showError('Password harus sama!')
    return
  }
  
  $.ajax({
      async: true,
      url: `${API_URL}/admin/password`,
      type: 'PUT',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify({
        password
      }),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        showError(response.message)
        endLoadingButton('#change-password-button', 'Simpan')
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      success: function(res) {
        showSuccess(res.message)
        endLoadingButton('#change-password-button', 'Simpan')
        $('#change-password-modal').modal('hide')
      }
  });
})

function getConfig(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let config = res.data
      if(config.web_logo){
        $('.brand-image').prop("src", WEB_URL+"/"+config.web_logo)
      }
      if(config.web_icon){
        $("#favicon").attr("href", WEB_URL+"/"+config.web_icon);
      }
    }
  });
}