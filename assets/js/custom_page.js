let PAGE_ID

$(document).ready(function(){
  //render datatable
  let page_table = $('#page-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: PAGE_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "title",
            orderable: false
          },
          { 
            data: "alias",
            orderable: false
          },
          { 
            data: "position",
            orderable: false
          },
          { 
            data: "is_active",
            orderable: false,
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'success' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<button class="btn btn-sm btn-success page-update-toggle" data-id="${data}" data-toggle="modal" data-target="#page-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>`

                if(parseInt(row.is_active)){
                  actions += ` <button class="btn btn-sm btn-warning page-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#page-inactive-modal" title="inactive">
                    <i class="fas fa-ban"></i>
                  </button>`
                }else{
                  actions += ` <button class="btn btn-sm btn-info page-active-toggle" data-id="${data}" data-toggle="modal" data-target="#page-active-modal" title="active">
                    <i class="fas fa-check"></i>
                  </button>
                  <button class="btn btn-sm btn-danger page-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#page-delete-modal" title="delete">
                    <i class="fas fa-trash"></i>
                  </button>`
                }

                return actions
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#page-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".page-update-toggle", "click", function(e) {
    PAGE_ID = $(this).data('id')
    clearForm('update')

    $('#page-update-overlay').show()
    $.ajax({
        async: true,
        url: `${PAGE_API_URL}/id/${PAGE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#page-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#page-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".page-delete-toggle", "click", function(e) {
    PAGE_ID = $(this).data('id')
    console.log("ID:", PAGE_ID)
  })

  $("body").delegate(".page-inactive-toggle", "click", function(e) {
    PAGE_ID = $(this).data('id')
    console.log("ID:", PAGE_ID)
  })

  $("body").delegate(".page-active-toggle", "click", function(e) {
    PAGE_ID = $(this).data('id')
    console.log("ID:", PAGE_ID)
  })

  //submit form
  $('#page-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#page-create-button')
    
    let $form = $(this)
    let data = {
      title:  $form.find( "input[name='title']" ).val(),
      content: $('.rich-text-create').summernote('code'),
      position: $('#page-position-create-field').find(":selected").val()
    }
    $.ajax({
        async: true,
        url: PAGE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#page-create-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#page-create-button', 'Submit')
          showSuccess(res.message)
          $('#page-create-modal').modal('hide')
          page_table.ajax.reload()
        }
    });
  })
  
  $('#page-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#page-update-button')

    let $form = $(this)
    let data = {
      id: PAGE_ID,
      title:  $form.find( "input[name='title']" ).val(),
      content:  $('.rich-text-update').summernote('code'),
      position: $('#page-position-update-field').find(":selected").val()
    }
    console.log("REQUEST: ", data)
    $.ajax({
        async: true,
        url: PAGE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#page-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#page-update-button', 'Submit')
          showSuccess(res.message)
          $('#page-update-modal').modal('toggle')
          page_table.ajax.reload()
        }
    });
  })
  
  $('#page-delete-button').click(function (){
    startLoadingButton('#page-delete-button')
    
    $.ajax({
        async: true,
        url: `${PAGE_API_URL}/delete/${PAGE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#page-delete-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#page-delete-button', 'Yes')
          showSuccess(res.message)
          $('#page-delete-modal').modal('toggle')
          page_table.ajax.reload()
        }
    });
  })

  $('#page-inactive-button').click(function (){
    startLoadingButton('#page-inactive-button')
    
    $.ajax({
        async: true,
        url: `${PAGE_API_URL}/inactive/${PAGE_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#page-inactive-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#page-inactive-button', 'Yes')
          showSuccess(res.message)
          $('#page-inactive-modal').modal('toggle')
          page_table.ajax.reload()
        }
    });
  })

  $('#page-active-button').click(function (){
    startLoadingButton('#page-active-button')
    
    $.ajax({
        async: true,
        url: `${PAGE_API_URL}/active/${PAGE_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#page-active-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#page-active-button', 'Yes')
          showSuccess(res.message)
          $('#page-active-modal').modal('toggle')
          page_table.ajax.reload()
        }
    });
  })
})

$('#page-title-update-field').change(function(){
  let val = $(this).val()
  let alias = val.toLowerCase().replace(" ", "-")
  $('#page-alias-update-field').val(alias)
})

function renderForm(data, type){
  let $form = $(`#page-${type}-form`)
  $form.find( "input[name='title']" ).val(data.title)
  $(`#page-position-${type}-field`).val(data.position).change()
  $(`.rich-text-${type}`).summernote("code", data.content)
  if(type == "update"){
    $form.find( "input[name='alias']" ).val(data.alias)
  }
}

function clearForm(type){
  let $form = $(`#page-${type}-form`)
  $form.find( "input[name='title']" ).val("")
  $(`.rich-text-${type}`).summernote("code", "")
}
