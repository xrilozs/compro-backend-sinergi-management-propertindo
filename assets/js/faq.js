let FAQ_ID;

$(document).ready(function(){    
  let faq_table = $('#faq-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: FAQ_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          return d
        },
        error: function(res){
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "question",
            orderable: false
          },
          { 
            data: "answer",
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                return `<button class="btn btn-sm btn-success faq-update-toggle" data-id="${data}" data-toggle="modal" data-target="#faq-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger faq-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#faq-delete-modal" title="delete">
                  <i class="fas fa-trash"></i>
                </button>`
            },
            orderable: false
          }
      ]
  });

  $('#faq-create-toggle').click(function(){
    clearForm('create')
  })
  
  $("body").delegate(".faq-update-toggle", "click", function(e) {
    FAQ_ID = $(this).data('id')
    clearForm("update")
    console.log("ID:", FAQ_ID)

    $('#faq-update-overlay').show()
    $.ajax({
        async: true,
        url: `${FAQ_API_URL}/id/${FAQ_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#faq-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#faq-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".faq-delete-toggle", "click", function(e) {
    FAQ_ID = $(this).data('id')
    console.log("ID:", FAQ_ID)
  })
  
  $('#faq-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton("#faq-create-button")
    
    let $form = $( this ),
        question = $form.find( "textarea[name='question']" ).val(),
        answer = $form.find( "textarea[name='answer']" ).val()

    $.ajax({
        async: true,
        url: FAQ_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify({
          question: question,
          answer: answer
        }),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#faq-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#faq-create-button', 'Simpan')
          showSuccess(res.message)
          $('#faq-create-modal').modal('hide')
          faq_table.ajax.reload()
        }
    });
  })
  
  $('#faq-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#faq-update-button')
    let $form = $(this),
    question = $form.find( "textarea[name='question']" ).val(),
    answer = $form.find( "textarea[name='answer']" ).val()

    $.ajax({
        async: true,
        url: FAQ_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify({
          id: FAQ_ID,
          question: question,
          answer: answer
        }),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#faq-update-button')
        },
        success: function(res) {
          endLoadingButton('#faq-update-button', 'Simpan')
          showSuccess(res.message)
          $('#faq-update-modal').modal('toggle')
          faq_table.ajax.reload()
        }
    });
  })
  
  $('#faq-delete-button').click(function (){
    startLoadingButton('#faq-delete-button')
    
    $.ajax({
        async: true,
        url: `${FAQ_API_URL}/delete/${FAQ_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#faq-delete-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#faq-delete-button', 'Simpan')
          showSuccess(res.message)
          $('#faq-delete-modal').modal('toggle')
          faq_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#faq-${type}-form`)
  $form.find( "textarea[name='question']" ).val(data.question)
  $form.find( "textarea[name='answer']" ).val(data.answer)
}

function clearForm(type){
  let $form = $(`#faq-${type}-form`)
  $form.find( "textarea[name='question']" ).val("")
  $form.find( "textarea[name='answer']" ).val("")
}