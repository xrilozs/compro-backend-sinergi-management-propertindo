let MESSAGE_ID,
    message_table

$(document).ready(function(){

  //render datatable
  message_table = $('#message-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      ajax: {
        async: true,
        url: MESSAGE_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "fullname",
          },
          { 
            data: "phone",
          },
          { 
            data: "email",
          },
          { 
            data: "company",
          },
          {
            data: "product",
            render: function (data, type, row, meta) {
              return data ? `<a href="${WEB_URL+"/product/detail/"+row.product_id}" target="_blank">${data}</a>` : "-"
            },
          },
          {
            data: "service",
            render: function (data, type, row, meta) {
              return data ? `<a href="${WEB_URL+"/service/detail/"+row.service_id}" target="_blank">${data}</a>` : "-"
            },
          },
          { 
            data: "is_read",
            orderable: false,
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'default' : 'primary'
              let status = parseInt(data) ? 'SUDAH DIBACA' : 'BELUM DIBACA'
              let badge = `<span class="badge badge-pill badge-${color}">${status}</span>`

              return badge
            }
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<button class="btn btn-sm btn-default message-detail-toggle" data-id="${data}" data-toggle="modal" data-target="#message-detail-modal" title="baca pesan">
                  <i class="fas fa-envelope-open-text"></i>
                </button>`

                return actions
            },
            orderable: false
          }
      ]
  });
  
  //button action click  
  $("body").delegate(".message-detail-toggle", "click", function(e) {
    MESSAGE_ID = $(this).data('id')
    clearForm('detail')

    $('#message-update-overlay').show()
    $.ajax({
        async: true,
        url: `${MESSAGE_API_URL}/id/${MESSAGE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#message-detail-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#message-detail-overlay').hide()
          renderForm(response, 'detail')
          if(!parseInt(response.is_read)){
            readMessage()
          }
        }
    });
  })
})

function readMessage(){
  $.ajax({
    async: true,
    url: `${MESSAGE_API_URL}/read/${MESSAGE_ID}`,
    type: 'POST',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      message_table.ajax.reload()
    }
  });
}

function renderForm(data, type){
  let $form = $(`#message-${type}-form`)
  $form.find( "input[name='fullname']" ).val(data.fullname)
  $form.find( "input[name='phone']" ).val(data.phone)
  $form.find( "input[name='email']" ).val(data.email)
  $form.find( "input[name='company']" ).val(data.company)
  $form.find( "input[name='createdat']" ).val(data.created_at)
  $form.find( "textarea[name='content']" ).val(data.content)
  if(data.product_id){
    $('#message-product-section').css('display', 'block')
    $('#message-product-detail-title').html(`<a href="${WEB_URL+"/product/detail/"+data.product_id}">${data.product}</a>`)
    $('#message-product-detail-image').html(`<img src="${API_URL+"/"+data.product_image}" class="img-fluid" style="width:200px;">`)
  }else{
    $('#message-product-section').css('display', 'none')
  }

  if(data.service_id){
    $('#message-service-section').css('display', 'block')
    $('#message-service-detail-title').html(`<a href="${WEB_URL+"/service/detail/"+data.service_id}">${data.service}</a>`)
    $('#message-service-detail-description').html(`<p>${data.service_description}</p>`)
  }else{
    $('#message-service-section').css('display', 'none')
  }
}

function clearForm(type){
  let $form = $(`#message-${type}-form`)
  $form.find( "input[name='fullname']" ).val("")
  $form.find( "input[name='phone']" ).val("")
  $form.find( "input[name='email']" ).val("")
  $form.find( "input[name='company']" ).val("")
  $form.find( "input[name='createdat']" ).val("")
  $form.find( "textarea[name='content']" ).val("")
  $('#message-product-section').css('display', 'none')
  $('#message-service-section').css('display', 'none')
}
