let CONFIG_ID;
let LOGO_URL;
let ICON_URL;

let emailDOM = $('#email')[0]
let phoneDOM = $('#phone')[0]
const emailTagify = new Tagify(emailDOM);
const phoneTagify = new Tagify(phoneDOM);

$(document).ready(function(){
  getWebConfig()

  $('#config-upload-logo').change(function(e) {
    let file = e.target.files[0];
    upload_image(file)
  });

  $('#config-upload-icon').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, true)
  });
  
  $('#config-button').click(function (){
    startLoadingButton("#config-button")

    const emailTagValues = emailTagify.value;
    const phoneTagValues = phoneTagify.value;
    
    let contactForm = $('#contact-form'),
        socialForm  = $('#social-form'),
        appForm     = $('#application-form'),
        request = {
          id: CONFIG_ID,
          sm_whatsapp: contactForm.find( "input[name='whatsapp']" ).val(),
          email: emailTagValues.map(tag => tag.value).join(","),
          phone: phoneTagValues.map(tag => tag.value).join(","),
          address: contactForm.find( "textarea[name='address']" ).val(),
          sm_facebook: socialForm.find( "input[name='sm_facebook']" ).val(),
          sm_twitter: socialForm.find( "input[name='sm_twitter']" ).val(),
          sm_instagram: socialForm.find( "input[name='sm_instagram']" ).val(),
          web_description: appForm.find( "textarea[name='web-description']" ).val(),
          web_logo: LOGO_URL,
          web_icon: ICON_URL,
        }

    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#config-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#config-button', 'Simpan')
          showSuccess(res.message)
          getWebConfig()
        }
    });
  })
})

function getWebConfig(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else{
        renderConfigFormBlank()
      }
    },
    success: function(res) {
      CONFIG_ID = res.data.id
      renderContactForm(res.data)
      renderSocialForm(res.data)
      renderApplicationForm(res.data)
    }
  });
}

function renderContactForm(data){
  let emailArr = data.email.split(",")
  let phoneArr = data.phone.split(",")
  emailTagify.addTags(emailArr);
  phoneTagify.addTags(phoneArr);

  let $form = $(`#contact-form`)
  $form.find( "input[name='whatsapp']" ).val(data.sm_whatsapp)
  $form.find( "textarea[name='address']" ).val(data.address)
}

function renderSocialForm(data){
  let $form = $(`#social-form`)
  $form.find( "input[name='sm_facebook']" ).val(data.sm_facebook)
  $form.find( "input[name='sm_twitter']" ).val(data.sm_twitter)
  $form.find( "input[name='sm_instagram']" ).val(data.sm_instagram)
}

function renderApplicationForm(data){
  let $form = $(`#application-form`)
  $form.find( "textarea[name='web-description']" ).val(data.web_description)
  LOGO_URL = data.web_logo
  ICON_URL = data.web_icon
  console.log(`${WEB_URL}/${data.web_logo}`)
  if(LOGO_URL){
    $('#config-upload-logo').attr("data-default-file", `${WEB_URL}/${data.web_logo}`);
  }
  $('#config-upload-logo').dropify()

  if(ICON_URL){
    $('#config-upload-icon').attr("data-default-file", `${WEB_URL}/${data.web_icon}`);
  }
  $('#config-upload-icon').dropify()
}

//upload image
function upload_image(file, isIcon=false){
  $(`#config-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${CONFIG_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#config-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        if(isIcon){
          ICON_URL = response.file_url
        }else{
          LOGO_URL = response.file_url
        }
        $(`#config-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderConfigFormBlank(){
  console.log("render blank")
  $('#config-upload-logo').dropify()
  $('#config-upload-icon').dropify()
}