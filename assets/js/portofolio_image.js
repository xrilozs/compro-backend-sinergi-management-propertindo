let PORTOFOLIO_ID = window.location.pathname.split('/').pop(),
    PORTOFOLIO_IMAGE_ID,
    IMG_URL

$(document).ready(function(){
    getPortofolioImage()

    $('#image-add-input').dropify()

    $('#image-add-input').change(function(e) {
        let file = e.target.files[0];
        upload_image(file)
    });

    $("body").delegate(".image-remove-toggle", "click", function(e) {
        PORTOFOLIO_IMAGE_ID = $(this).data("id")
    })

    $('#image-add-form').submit(function(e){
        e.preventDefault()
        startLoadingButton('#image-add-button')

        let data = {
            portofolio_id: PORTOFOLIO_ID,
            img_url: IMG_URL
        }

        $.ajax({
            async: true,
            url: `${PORTOFOLIO_IMAGE_API_URL}`,
            type: 'POST',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              showSuccess(res.message)
              endLoadingButton('#image-add-button', 'Tambah')
              $('#image-add-modal').modal('toggle')
              $('#portofolio-image-loading').show()
              getPortofolioImage()
            }
        });
    })

    $('#image-delete-button').click(function (){
        startLoadingButton('#image-delete-button')
        
        $.ajax({
            async: true,
            url: `${PORTOFOLIO_IMAGE_API_URL}/delete/${PORTOFOLIO_IMAGE_ID}`,
            type: 'DELETE',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#image-delete-button', 'Ya')
            },
            success: function(res) {
              endLoadingButton('#image-delete-button', 'Ya')
              showSuccess(res.message)
              $('#image-delete-modal').modal('toggle')
              $('#portofolio-image-loading').show()
              getPortofolioImage()
            }
        });
      })
})

function upload_image(file){
    $(`#image-add-button`).attr("disabled", true)
  
    let formData = new FormData();
    formData.append('image', file);
    
    $.ajax({
        async: true,
        url: `${PORTOFOLIO_IMAGE_API_URL}/upload`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: formData,
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Upload gambar", "Gagal melakukan upload gambar!")
            $(`#image-add-button`).attr("disabled", false)
          }
        },
        success: function(res) {
          const response = res.data
          IMG_URL = response.file_url
          $(`#image-add-button`).attr("disabled", false)
          showSuccess(res.message)
        }
    });
}

function getPortofolioImage(){
    $.ajax({
        async: true,
        url: `${PORTOFOLIO_IMAGE_API_URL}/${PORTOFOLIO_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          $('#portofolio-image-loading').hide()
          renderImage(response)
        }
    });
}

function renderImage(data){
    // const total = data.length
    const grid  = 3

    let imageHtml = ""
    data.forEach(item => {
        let itemHtml = `<div class="col-${grid}">
            <img src="${WEB_URL+"/"+item.img_url}" style="width:100%; height:200px; object-fit:cover;">
            <button class="btn btn-danger btn-block mt-2 image-remove-toggle" data-id="${item.id}" data-toggle="modal" data-target="#image-delete-modal">
                <i class="fas fa-trash"></i> Hapus
            </button>
        </div>`
        imageHtml += itemHtml
    });

    if(!imageHtml){
        imageHtml = `<div class="col-12 text-center font-weight-bold">Belum Ada Gambar</div>`
    }

    $('#portofolio-image-section').html(imageHtml)
}
