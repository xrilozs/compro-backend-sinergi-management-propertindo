let PORTOFOLIO_ID

$(document).ready(function(){
  //render datatable
  let portofolio_table = $('#portofolio-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url:PORTOFOLIO_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "title",
            orderable: false
          },          
          { 
            data: "category",
            render: function (data, type, row, meta) {
              let color = data == 'SERVICE' ? 'info' : 'success'
              let badge = `<span class="badge badge-${color}">${data}</span>`

              return badge
            },
            orderable: false
          },
          { 
            data: "content",
            orderable: false,
            render: function (data, type, row, meta) {
              return sortText(data, 50, true)
            }
          },
          { 
            data: "date",
            orderable: false
          },
          { 
            data: "client",
            orderable: false
          },
          { 
            data: "is_active",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'success' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-${color}">${text}</span>`

              return badge
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let isActive = parseInt(row.is_active)
              let button = `<button class="btn btn-sm btn-success portofolio-update-toggle" data-id="${data}" data-toggle="modal" data-target="#portofolio-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-${getActionColor(isActive)} portofolio-${getAction(isActive)}-toggle" data-id="${data}" data-toggle="modal" data-target="#portofolio-${getAction(isActive)}-modal" title="${getAction(isActive)}">
                <i class="fas fa-${getActionIcon(isActive)}"></i>
              </button>`

              if(!isActive){
                button += `<button class="btn btn-sm btn-danger portofolio-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#portofolio-delete-modal" title="hapus servis">
                  <i class="fas fa-trash"></i>
                </button>`
              }

              button += `<a href="${WEB_URL+"/portofolio/image/"+data}" class="btn btn-sm btn-dark" title="gambar">
                <i class="fas fa-image"></i>
              </button>`

              return button
            },
            orderable: false
          }
      ]
  });

  
  //button action click
  $("#portofolio-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".portofolio-update-toggle", "click", function(e) {
    PORTOFOLIO_ID = $(this).data('id')
    clearForm('update')

    $('#portofolio-update-overlay').show()
    $.ajax({
        async: true,
        url: `${PORTOFOLIO_API_URL}/id/${PORTOFOLIO_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#portofolio-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#portofolio-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".portofolio-delete-toggle", "click", function(e) {
    PORTOFOLIO_ID = $(this).data('id')
    console.log("ID:", PORTOFOLIO_ID)
  })

  $("body").delegate(".portofolio-inactive-toggle", "click", function(e) {
    PORTOFOLIO_ID = $(this).data('id')
    console.log("ID:", PORTOFOLIO_ID)
  })

  $("body").delegate(".portofolio-active-toggle", "click", function(e) {
    PORTOFOLIO_ID = $(this).data('id')
    console.log("ID:", PORTOFOLIO_ID)
  })

  //submit form
  $('#portofolio-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#portofolio-create-button')
    
    let $form = $(this)
    let request = {
      title: $form.find( "input[name='title']" ).val(),
      category: $("#portofolio-category-create-field").find(":selected").val(),
      content: $(".rich-text-create").summernote('code'),
      date: $form.find( "input[name='date']" ).val(),
      client: $form.find( "input[name='client']" ).val(),
    }
    console.log
    $.ajax({
        async: true,
        url:PORTOFOLIO_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#portofolio-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#portofolio-create-button', 'Simpan')
          showSuccess(res.message)
          $('#portofolio-create-modal').modal('hide')
          portofolio_table.ajax.reload()
        }
    });
  })
  
  $('#portofolio-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#portofolio-update-button')

    let $form = $(this)
    let data = {
      id: PORTOFOLIO_ID,
      title: $form.find( "input[name='title']" ).val(),
      category: $("#portofolio-category-update-field").find(":selected").val(),
      content: $(".rich-text-update").summernote('code'),
      date: $form.find( "input[name='date']" ).val(),
      client: $form.find( "input[name='client']" ).val(),
    }

    $.ajax({
        async: true,
        url:PORTOFOLIO_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#portofolio-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#portofolio-update-button', 'Simpan')
          showSuccess(res.message)
          $('#portofolio-update-modal').modal('toggle')
          portofolio_table.ajax.reload()
        }
    });
  })
  
  $('#portofolio-delete-button').click(function (){
    startLoadingButton('#portofolio-delete-button')
    
    $.ajax({
        async: true,
        url: `${PORTOFOLIO_API_URL}/delete/${PORTOFOLIO_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#portofolio-delete-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#portofolio-delete-button', 'Ya')
          showSuccess(res.message)
          $('#portofolio-delete-modal').modal('toggle')
          portofolio_table.ajax.reload()
        }
    });
  })

  $('#portofolio-inactive-button').click(function (){
    startLoadingButton('#portofolio-inactive-button')
    
    $.ajax({
        async: true,
        url: `${PORTOFOLIO_API_URL}/inactive/${PORTOFOLIO_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#portofolio-inactive-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#portofolio-inactive-button', 'Ya')
          showSuccess(res.message)
          $('#portofolio-inactive-modal').modal('toggle')
          portofolio_table.ajax.reload()
        }
    });
  })

  $('#portofolio-active-button').click(function (){
    startLoadingButton('#portofolio-active-button')
    
    $.ajax({
        async: true,
        url: `${PORTOFOLIO_API_URL}/active/${PORTOFOLIO_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#portofolio-active-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#portofolio-active-button', 'Ya')
          showSuccess(res.message)
          $('#portofolio-active-modal').modal('toggle')
          portofolio_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#portofolio-${type}-form`)
  $form.find( "input[name='title']" ).val(data.title)
  $(`#portofolio-category-${type}-field`).val(data.category).change()
  $(`.rich-text-${type}`).summernote("code", data.content)
  $form.find( "input[name='date']" ).val(data.date)
  $form.find( "input[name='client']" ).val(data.client)
}

function clearForm(type){
  let $form = $(`#portofolio-${type}-form`)
  $form.find( "input[name='title']" ).val("")
  $(`.rich-text-${type}`).summernote("code", "")
  $form.find( "input[name='date']" ).val("")
  $form.find( "input[name='client']" ).val("")
}

function getAction(isActive){
  return parseInt(isActive) ? 'inactive' : 'active'
}

function getActionIcon(isActive){
  return parseInt(isActive) ? 'times' : 'check'
}

function getActionColor(isActive){
  return parseInt(isActive) ? 'warning' : 'info'
}