let PRODUCT_ID

$(document).ready(function(){
  getCategory()

  //render datatable
  let product_table = $('#product-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url:PRODUCT_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name",
            orderable: false
          },
          { 
            data: "description",
            orderable: false,
            render: function (data, type, row, meta) {
              return sortText(data, 50, true)
            }
          },
          // { 
          //   data: "price",
          //   render: function (data, type, row, meta) {
          //     return formatRupiah(data)
          //   },
          //   orderable: false
          // },
          { 
            data: "category",
            orderable: false
          },
          { 
            data: "is_active",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'success' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-${color}">${text}</span>`

              return badge
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let isActive = parseInt(row.is_active)
              let button = `<button class="btn btn-sm btn-success product-update-toggle" data-id="${data}" data-toggle="modal" data-target="#product-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-${getActionColor(isActive)} product-${getAction(isActive)}-toggle" data-id="${data}" data-toggle="modal" data-target="#product-${getAction(isActive)}-modal" title="${getAction(isActive)}">
                <i class="fas fa-${getActionIcon(isActive)}"></i>
              </button>`

              if(!isActive){
                button += `<button class="btn btn-sm btn-danger product-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#product-delete-modal" title="hapus servis">
                  <i class="fas fa-trash"></i>
                </button>`
              }

              button += `<a href="${WEB_URL+"/product/image/"+data}" class="btn btn-sm btn-dark" title="gambar">
                <i class="fas fa-image"></i>
              </button>`

              return button
            },
            orderable: false
          }
      ]
  });

  
  //button action click
  $("#product-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".product-update-toggle", "click", function(e) {
    PRODUCT_ID = $(this).data('id')
    clearForm('update')

    $('#product-update-overlay').show()
    $.ajax({
        async: true,
        url: `${PRODUCT_API_URL}/id/${PRODUCT_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#product-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#product-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".product-delete-toggle", "click", function(e) {
    PRODUCT_ID = $(this).data('id')
    console.log("ID:", PRODUCT_ID)
  })

  $("body").delegate(".product-inactive-toggle", "click", function(e) {
    PRODUCT_ID = $(this).data('id')
    console.log("ID:", PRODUCT_ID)
  })

  $("body").delegate(".product-active-toggle", "click", function(e) {
    PRODUCT_ID = $(this).data('id')
    console.log("ID:", PRODUCT_ID)
  })

  //submit form
  $('#product-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#product-create-button')
    
    let $form = $(this)
    let request = {
      name: $form.find( "input[name='name']" ).val(),
      description: $(".rich-text-create").summernote('code'),
      // price: $form.find( "input[name='price']" ).val(),
      category_id: $("#product-category-create-field").find(":selected").val()
    }
    console.log
    $.ajax({
        async: true,
        url:PRODUCT_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#product-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#product-create-button', 'Simpan')
          showSuccess(res.message)
          $('#product-create-modal').modal('hide')
          product_table.ajax.reload()
        }
    });
  })
  
  $('#product-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#product-update-button')

    let $form = $(this)
    let data = {
      id: PRODUCT_ID,
      name: $form.find( "input[name='name']" ).val(),
      description: $(".rich-text-update").summernote('code'),
      // price: $form.find( "input[name='price']" ).val(),
      category_id: $("#product-category-update-field").find(":selected").val()
    }

    $.ajax({
        async: true,
        url:PRODUCT_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#product-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#product-update-button', 'Simpan')
          showSuccess(res.message)
          $('#product-update-modal').modal('toggle')
          product_table.ajax.reload()
        }
    });
  })
  
  $('#product-delete-button').click(function (){
    startLoadingButton('#product-delete-button')
    
    $.ajax({
        async: true,
        url: `${PRODUCT_API_URL}/delete/${PRODUCT_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#product-delete-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#product-delete-button', 'Ya')
          showSuccess(res.message)
          $('#product-delete-modal').modal('toggle')
          product_table.ajax.reload()
        }
    });
  })

  $('#product-inactive-button').click(function (){
    startLoadingButton('#product-inactive-button')
    
    $.ajax({
        async: true,
        url: `${PRODUCT_API_URL}/inactive/${PRODUCT_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#product-inactive-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#product-inactive-button', 'Ya')
          showSuccess(res.message)
          $('#product-inactive-modal').modal('toggle')
          product_table.ajax.reload()
        }
    });
  })

  $('#product-active-button').click(function (){
    startLoadingButton('#product-active-button')
    
    $.ajax({
        async: true,
        url: `${PRODUCT_API_URL}/active/${PRODUCT_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#product-active-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#product-active-button', 'Ya')
          showSuccess(res.message)
          $('#product-active-modal').modal('toggle')
          product_table.ajax.reload()
        }
    });
  })
})

function getCategory(){
  $.ajax({
    async: true,
    url: `${PRODUCT_CATEGORY_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      renderCategoryOption(response)
    }
  });
}

function renderCategoryOption(data){
  let category_html = ""
  data.forEach(item => {
    let option_html = `<option value="${item.id}">${item.name}</option>`
    category_html += option_html
  });
  $(`.category-option`).html(category_html)
}

function renderForm(data, type){
  let $form = $(`#product-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  // $form.find( "input[name='price']" ).val(data.price)
  $(`.rich-text-${type}`).summernote("code", data.description)
  $(`#product-category-${type}-field`).val(category_id).change()
}

function clearForm(type){
  let $form = $(`#product-${type}-form`)
  $form.find( "input[name='title']" ).val("")
  $(`.rich-text-${type}`).summernote("code", "")
  $form.find( "input[name='date']" ).val("")
  $form.find( "input[name='client']" ).val("")
}

function getAction(isActive){
  return parseInt(isActive) ? 'inactive' : 'active'
}

function getActionIcon(isActive){
  return parseInt(isActive) ? 'times' : 'check'
}

function getActionColor(isActive){
  return parseInt(isActive) ? 'warning' : 'info'
}