let SERVICE_ID

$(document).ready(function(){
  //render datatable
  let service_table = $('#service-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url:SERVICE_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name",
            orderable: false
          },
          { 
            data: "description",
            orderable: false,
            render: function (data, type, row, meta) {
              return sortText(data, 50, true)
            }
          },
          { 
            data: "is_active",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'success' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let isActive = parseInt(row.is_active)
              let button = `<button class="btn btn-sm btn-success service-update-toggle" data-id="${data}" data-toggle="modal" data-target="#service-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-${getActionColor(isActive)} service-${getAction(isActive)}-toggle" data-id="${data}" data-toggle="modal" data-target="#service-${getAction(isActive)}-modal" title="${getAction(isActive)}">
                <i class="fas fa-${getActionIcon(isActive)}"></i>
              </button>`

              if(!isActive){
                button += `<button class="btn btn-sm btn-danger service-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#service-delete-modal" title="hapus servis">
                  <i class="fas fa-trash"></i>
                </button>`
              }
              return button
            },
            orderable: false
          }
      ]
  });

  
  //button action click
  $("#service-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".service-update-toggle", "click", function(e) {
    SERVICE_ID = $(this).data('id')
    clearForm('update')

    $('#service-update-overlay').show()
    $.ajax({
        async: true,
        url: `${SERVICE_API_URL}/id/${SERVICE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#service-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#service-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".service-delete-toggle", "click", function(e) {
    SERVICE_ID = $(this).data('id')
    console.log("ID:", SERVICE_ID)
  })

  $("body").delegate(".service-inactive-toggle", "click", function(e) {
    SERVICE_ID = $(this).data('id')
    console.log("ID:", SERVICE_ID)
  })

  $("body").delegate(".service-active-toggle", "click", function(e) {
    SERVICE_ID = $(this).data('id')
    console.log("ID:", SERVICE_ID)
  })

  //submit form
  $('#service-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#service-create-button')
    
    let $form = $(this)
    let request = {
      name: $form.find( "input[name='name']" ).val(),
      description: $(".rich-text-create").summernote('code')
    }
    console.log
    $.ajax({
        async: true,
        url:SERVICE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#service-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#service-create-button', 'Simpan')
          showSuccess(res.message)
          $('#service-create-modal').modal('hide')
          service_table.ajax.reload()
        }
    });
  })
  
  $('#service-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#service-update-button')

    let $form = $(this)
    let data = {
      id: SERVICE_ID,
      name: $form.find( "input[name='name']" ).val(),
      description: $(".rich-text-update").summernote('code'),
    }

    $.ajax({
        async: true,
        url:SERVICE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#service-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#service-update-button', 'Simpan')
          showSuccess(res.message)
          $('#service-update-modal').modal('toggle')
          service_table.ajax.reload()
        }
    });
  })
  
  $('#service-delete-button').click(function (){
    startLoadingButton('#service-delete-button')
    
    $.ajax({
        async: true,
        url: `${SERVICE_API_URL}/delete/${SERVICE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#service-delete-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#service-delete-button', 'Ya')
          showSuccess(res.message)
          $('#service-delete-modal').modal('toggle')
          service_table.ajax.reload()
        }
    });
  })

  $('#service-inactive-button').click(function (){
    startLoadingButton('#service-inactive-button')
    
    $.ajax({
        async: true,
        url: `${SERVICE_API_URL}/inactive/${SERVICE_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#service-inactive-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#service-inactive-button', 'Ya')
          showSuccess(res.message)
          $('#service-inactive-modal').modal('toggle')
          service_table.ajax.reload()
        }
    });
  })

  $('#service-active-button').click(function (){
    startLoadingButton('#service-active-button')
    
    $.ajax({
        async: true,
        url: `${SERVICE_API_URL}/active/${SERVICE_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#service-active-button', 'Ya')
        },
        success: function(res) {
          endLoadingButton('#service-active-button', 'Ya')
          showSuccess(res.message)
          $('#service-active-modal').modal('toggle')
          service_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#service-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $(`.rich-text-${type}`).summernote("code", data.description)
}

function clearForm(type){
  let $form = $(`#service-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $(`.rich-text-${type}`).summernote("code", "")
}

function getAction(isActive){
  return parseInt(isActive) ? 'inactive' : 'active'
}

function getActionIcon(isActive){
  return parseInt(isActive) ? 'times' : 'check'
}

function getActionColor(isActive){
  return parseInt(isActive) ? 'warning' : 'info'
}