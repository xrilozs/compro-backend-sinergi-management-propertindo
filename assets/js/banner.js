let BANNER_ID,
    IMG_URL

$(document).ready(function(){
  $('#banner-create-image').dropify()

  //render datatable
  let banner_table = $('#banner-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: BANNER_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          {
            data: "img_url",
            render: function (data, type, row, meta) {
              return `<img src="${WEB_URL+"/"+data}" class="img-responsive" style="width:200px;">`
            }
          },
          { 
            data: "order_no",
            orderable: false
          },
          { 
            data: "description",
            render: function (data, type, row, meta) {
              return sortText(data, 50)
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let isActive = row.is_active
                return `<button class="btn btn-sm btn-success banner-update-toggle" data-id="${data}" data-toggle="modal" data-target="#banner-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger banner-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#banner-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`
            },
            orderable: false
          }
      ]
  });

  $('#banner-create-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "create")
  });

  $('#banner-update-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "update")
  });
  
  //button action click
  $("#banner-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".banner-update-toggle", "click", function(e) {
    BANNER_ID = $(this).data('id')
    clearForm('update')

    $('#banner-update-overlay').show()
    $.ajax({
        async: true,
        url: `${BANNER_API_URL}/id/${BANNER_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#banner-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#banner-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".banner-delete-toggle", "click", function(e) {
    BANNER_ID = $(this).data('id')
    console.log("ID:", BANNER_ID)
  })

  //submit form
  $('#banner-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#banner-create-button')
    
    let $form = $(this)
    let data = {
      order_no:  $form.find( "input[name='order_no']" ).val(),
      description:  $form.find( "textarea[name='description']" ).val(),
      img_url: IMG_URL
    }
    $.ajax({
        async: true,
        url: BANNER_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#banner-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#banner-create-button', 'Simpan')
          showSuccess(res.message)
          $('#banner-create-modal').modal('hide')
          banner_table.ajax.reload()
        }
    });
  })
  
  $('#banner-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#banner-update-button')

    let $form = $(this)
    let data = {
      id: BANNER_ID,
      order_no:  $form.find( "input[name='order_no']" ).val(),
      description:  $form.find( "textarea[name='description']" ).val(),
      img_url: IMG_URL
    }

    $.ajax({
        async: true,
        url: BANNER_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#banner-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#banner-update-button', 'Simpan')
          showSuccess(res.message)
          $('#banner-update-modal').modal('toggle')
          banner_table.ajax.reload()
        }
    });
  })
  
  $('#banner-delete-button').click(function (){
    startLoadingButton('#banner-delete-button')
    
    $.ajax({
        async: true,
        url: `${BANNER_API_URL}/delete/${BANNER_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#banner-delete-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#banner-delete-button', 'Yes')
          showSuccess(res.message)
          $('#banner-delete-modal').modal('toggle')
          banner_table.ajax.reload()
        }
    });
  })
})

function upload_image(file, type){
  $(`#banner-${type}-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${BANNER_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#banner-${type}-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        IMG_URL = response.file_url
        $(`#banner-${type}-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderForm(data, type){
  let $form = $(`#banner-${type}-form`)
  $form.find( "input[name='order_no']" ).val(data.order_no)
  $form.find( "textarea[name='description']" ).val(data.description)
  if(type == 'update'){
    // $('#banner-update-image').attr("data-default-file", `${WEB_URL}/${data.img_url}`);
    // $('#banner-update-image').dropify()
    var drEvent = $('#banner-update-image').dropify({
      defaultFile: `${WEB_URL}/${data.img_url}`
    });
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    drEvent.settings.defaultFile = `${WEB_URL}/${data.img_url}`;
    drEvent.destroy();
    drEvent.init();
    IMG_URL = data.img_url
  }
}

function clearForm(type){
  let $form = $(`#banner-${type}-form`)
  $form.find( "input[name='order_no']" ).val("")
  $form.find( "textarea[name='description']" ).val("")
}
