let CATEGORY_ID,
    IMG_URL

$(document).ready(function(){
  $('#category-create-image').dropify()

  //render datatable
  let category_table = $('#category-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: PRODUCT_CATEGORY_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          {
            data: "img_url",
            render: function (data, type, row, meta) {
              return `<img src="${WEB_URL+"/"+data}" class="img-responsive" style="width:200px;">`
            }
          },
          { 
            data: "name",
            orderable: false
          },
          { 
            data: "is_active",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'success' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let isActive = row.is_active
                return `<button class="btn btn-sm btn-success category-update-toggle" data-id="${data}" data-toggle="modal" data-target="#category-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-${getActionColor(isActive)} category-${getAction(isActive)}-toggle" data-id="${data}" data-toggle="modal" data-target="#category-${getAction(isActive)}-modal" title="${getAction(isActive)}">
                  <i class="fas fa-${getActionIcon(isActive)}"></i>
                </button>`
            },
            orderable: false
          }
      ]
  });

  $('#category-create-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "create")
  });

  $('#category-update-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "update")
  });
  
  //button action click
  $("#category-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".category-update-toggle", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
    clearForm('update')

    $('#category-update-overlay').show()
    $.ajax({
        async: true,
        url: `${PRODUCT_CATEGORY_API_URL}/id/${CATEGORY_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#category-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#category-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".category-inactive-toggle", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
    console.log("ID:", CATEGORY_ID)
  })

  $("body").delegate(".category-active-toggle", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
    console.log("ID:", CATEGORY_ID)
  })

  //submit form
  $('#category-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#category-create-button')
    
    let $form = $(this)
    let data = {
      name:  $form.find( "input[name='name']" ).val(),
      img_url: IMG_URL
    }
    $.ajax({
        async: true,
        url: PRODUCT_CATEGORY_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#category-create-button', 'Simpan')
          showSuccess(res.message)
          $('#category-create-modal').modal('hide')
          category_table.ajax.reload()
        }
    });
  })
  
  $('#category-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#category-update-button')

    let $form = $(this)
    let data = {
      id: CATEGORY_ID,
      name:  $form.find( "input[name='name']" ).val(),
      img_url: IMG_URL
    }

    $.ajax({
        async: true,
        url: PRODUCT_CATEGORY_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#category-update-button', 'Simpan')
          showSuccess(res.message)
          $('#category-update-modal').modal('toggle')
          category_table.ajax.reload()
        }
    });
  })
  
  $('#category-inactive-button').click(function (){
    startLoadingButton('#category-inactive-button')
    
    $.ajax({
        async: true,
        url: `${PRODUCT_CATEGORY_API_URL}/inactive/${CATEGORY_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-inactive-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#category-inactive-button', 'Yes')
          showSuccess(res.message)
          $('#category-inactive-modal').modal('toggle')
          category_table.ajax.reload()
        }
    });
  })

  $('#category-active-button').click(function (){
    startLoadingButton('#category-active-button')
    
    $.ajax({
        async: true,
        url: `${PRODUCT_CATEGORY_API_URL}/active/${CATEGORY_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-active-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#category-active-button', 'Yes')
          showSuccess(res.message)
          $('#category-active-modal').modal('toggle')
          category_table.ajax.reload()
        }
    });
  })
})

function upload_image(file, type){
  $(`#category-${type}-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${PRODUCT_CATEGORY_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#category-${type}-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        IMG_URL = response.file_url
        $(`#category-${type}-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderForm(data, type){
  let $form = $(`#category-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  if(type == 'update'){
    // $('#category-update-image').attr("data-default-file", `${WEB_URL}/${data.img_url}`);
    // $('#category-update-image').dropify()
    var drEvent = $('#category-update-image').dropify({
      defaultFile: `${WEB_URL}/${data.img_url}`
    });
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    drEvent.settings.defaultFile = `${WEB_URL}/${data.img_url}`;
    drEvent.destroy();
    drEvent.init();
    IMG_URL = data.img_url
  }
}

function clearForm(type){
  let $form = $(`#category-${type}-form`)
  $form.find( "input[name='name']" ).val("")
}

function getAction(isActive){
  return parseInt(isActive) ? 'inactive' : 'active'
}

function getActionIcon(isActive){
  return parseInt(isActive) ? 'times' : 'check'
}

function getActionColor(isActive){
  return parseInt(isActive) ? 'danger' : 'info'
}