<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function product_list(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/product.js?v=').time()
			),
			"title"		=> "Produk"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/product/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function product_image(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/product_image.js?v=').time()
			),
			"title"		=> "Gambar Produk"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/product/image');
		$this->load->view('layout/main_footer', $data);
	}

	public function product_detail(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/product_detail.js?v=').time()
			),
			"title"		=> "Detail Produk"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/product/detail');
		$this->load->view('layout/main_footer', $data);
	}
}
