<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Faq extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/faq [GET]
    function get_faq(){
        #init req & resp
        $resp_obj   = new Response_api();
        $search     = $this->input->get('search');
        $draw       = $this->input->get('draw');

        #get faq
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $faq    = $this->faq_model->get_faqs($search, $order);
        $total  = $this->faq_model->count_faq($search);

        #response
        if(empty($draw)){
            $resp_obj->set_response(200, "success", "Get faq is success", $faq);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/api/faq [GET] - Get faq is success', $faq);
            $resp_obj->set_response_datatable(200, $faq, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /api/faq/id/$id [GET]
    function get_faq_by_id($id){
        $resp_obj = new Response_api();

        #get faq detail
        $faq = $this->faq_model->get_faq_by_id($id);
        if(is_null($faq)){
            logging('error', '/api/faq/id/'.$id.' [GET] - faq not found');
            $resp_obj->set_response(404, "failed", "faq not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/faq/id/'.$id.' [GET] - Get faq by id success', $faq);
        $resp_obj->set_response(200, "success", "Get faq by id success", $faq);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/faq [POST]
    function create_faq(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/faq [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('question', 'answer');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/faq [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']  = get_uniq_id();
        $flag           = $this->faq_model->create_faq($request);
        
        #response
        if(!$flag){
            logging('error', '/api/faq [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/faq [POST] - Create faq success', $request);
        $resp_obj->set_response(200, "success", "Create faq success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/faq [PUT]
    function update_faq(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/faq [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'question', 'answer');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/faq [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check faq
        $faq = $this->faq_model->get_faq_by_id($request['id']);
        if(is_null($faq)){
            logging('error', '/api/faq [PUT] - faq not found', $request);
            $resp_obj->set_response(404, "failed", "faq not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update faq
        $flag = $this->faq_model->update_faq($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('debug', '/api/faq [PUT] - Data does not change', $request);
            $resp_obj->set_response(200, "success", "Data does not change");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/faq [PUT] - Update faq success', $request);
        $resp_obj->set_response(200, "success", "Update faq success", $request);
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /api/faq/$id [DELETE]
    function delete_faq($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/faq/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check faq
        $faq = $this->faq_model->get_faq_by_id($id);
        if(is_null($faq)){
            logging('error', '/api/faq/'.$id.' [DELETE] - faq not found');
            $resp_obj->set_response(404, "failed", "faq not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete faq
        $flag = $this->faq_model->delete_faq($id);
        
        #response
        if(!$flag){
            logging('error', '/api/faq/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/faq/'.$id.' [DELETE] - Delete faq success');
        $resp_obj->set_response(200, "success", "Delete faq success");
        set_output($resp_obj->get_response());
        return;
    }
  
}