<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Config extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }
  
  #path: /config [GET]
  function get_config(){
    $resp = new Response_api();

    #get config
    $config = $this->config_model->get_config();
    if(is_null($config)){
        logging('error', '/config [GET] - config not found');
        $resp->set_response(404, "failed", "config not found");
        set_output($resp->get_response());
        return;
    }
    
    #response
    logging('debug', '/config [GET] - Get config success', $config);
    $resp->set_response(200, "success", "Get config success", $config);
    set_output($resp->get_response());
    return;
  }
  
  #path: /config [POST]
  function save_config(){
    $resp     = new Response_api();
    $request  = json_decode($this->input->raw_input_stream, true);

    #check token
    $header     = $this->input->request_headers();
    $resp_token = verify_admin_token($header);
    if($resp_token['status'] == 'failed'){
      logging('error', '/config/contact [PUT] - '.$resp_token['message']);
      set_output($resp_token);
      return;
    }

    #check request params
    $keys       = array('email', 'address', 'phone', 'web_description');
    $check_res  = check_parameter_by_keysV2($request, $keys);
    if(!$check_res['success']){
        logging('error', "config/contact [PUT] - ".$check_res['message'], $request);
        $resp->set_response(400, "failed", $check_res['message']);
        set_output($resp->get_response());
        return;
    }

    #check config
    $config     = $this->config_model->get_config();
    $config_id  = is_null($config) ? get_uniq_id() : $config->id;

    $data = array(
      'email'           => $request['email'],
      'address'         => $request['address'],
      'phone'           => $request['phone'],
      'web_description' => $request['web_description'],
      'sm_whatsapp'     => array_key_exists('sm_whatsapp', $request) ? $request['sm_whatsapp'] : null,
      'sm_twitter'      => array_key_exists('sm_twitter', $request) ? $request['sm_twitter'] : null,
      'sm_instagram'    => array_key_exists('sm_instagram', $request) ? $request['sm_instagram'] : null,
      'sm_facebook'     => array_key_exists('sm_facebook', $request) ? $request['sm_facebook'] : null,
      'web_logo'        => array_key_exists('web_logo', $request) ? $request['web_logo'] : null,
      'web_icon'        => array_key_exists('web_icon', $request) ? $request['web_icon'] : null
    );

    #update config
    $flag = 0;
    if($config){
      $flag = $this->config_model->update_config($data, $config_id);
    }else{
      $data['id'] = $config_id;
      $flag = $this->config_model->create_config($data);
    }
    if(empty($flag)){
        logging('debug', '/config [POST] - Data does not change', $request);
        $resp->set_response(400, "error", "Data does not change", $this->db->last_query());
        set_output($resp->get_response());
        return;
    }
    logging('debug', '/config [POST] - Save config success', $request);
    $resp->set_response(200, "success", "save config success", $request);
    set_output($resp->get_response());
    return;
  }
  
  #path: /config/upload [POST]
  function upload_config_image(){
    #init variable
    $resp_obj = new Response_api();

    #check token
    $header = $this->input->request_headers();
    $resp = verify_admin_token($header);
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload [POST] - '.$resp['message']);
        set_output($resp);
        return;
    }

    #check requested param
    $destination = 'assets/web/';
    if (empty($_FILES['image']['name'])) {
        logging('error', '/config/upload [POST] - Missing parameter. please check API documentation');
        $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
        set_output($resp_obj->get_response());
        return;   
    }

    #upload image
    $file = $_FILES['image'];
    $resp = _upload_file($file, $destination);

    #response
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload [POST] - '.$resp['message']);
        $resp_obj->set_response(400, "failed", $resp['message']);
        set_output($resp_obj->get_response());
        return; 
    }
    $data                   = $resp['data'];
    $data['full_file_url']  = base_url($data['file_url']);
    
    logging('debug', '/config/upload [POST] - Upload image success', $data);
    $resp_obj->set_response(200, "success", "Upload image success", $data);
    set_output($resp_obj->get_response());
    return; 
  }
}

?>