<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Sub_service extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/sub-service/service/(:any) [GET]
    function get_sub_service_by_service($service_id){
        #init req & resp
        $resp_obj   = new Response_api();
        $is_active  = $this->input->get('is_active');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get sub service
        $sub_service = $this->sub_service_model->get_sub_service(null, $service_id, $is_active);
        logging('debug', '/api/sub-service [GET] - Get sub service by service is success', $sub_service);
        $resp_obj->set_response(200, "success", "Get sub service by service is success", $sub_service);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/sub-service/id/$id [GET]
    function get_sub_service_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get sub service detail
        $sub_service = $this->sub_service_model->get_sub_service_by_id($id);
        if(is_null($sub_service)){
            logging('error', '/api/sub-service/id/'.$id.' [GET] - sub service not found');
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/sub-service/id/'.$id.' [GET] - Get sub service by id success', $sub_service);
        $resp_obj->set_response(200, "success", "Get sub service by id success", $sub_service);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/sub-service [POST]
    function create_sub_service(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('service_id', 'name', 'description', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/sub-service [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check service
        $service = $this->service_model->get_service_by_id($request['service_id']);
        if(is_null($service)){
            logging('error', '/api/sub-service [POST] - service not found', $request);
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #create sub service
        $request['id']  = get_uniq_id();
        $flag           = $this->sub_service_model->create_sub_service($request);
        
        #response
        if(!$flag){
            logging('error', '/api/sub-service [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/sub-service [POST] - Create sub service success', $request);
        $resp_obj->set_response(200, "success", "Create sub service success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/sub-service [PUT]
    function update_sub_service(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'service_id', 'name', 'description', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/sub-service [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check sub service
        $sub_service = $this->sub_service_model->get_sub_service_by_id($request['id']);
        if(is_null($sub_service)){
            logging('error', '/api/sub-service [PUT] - sub service not found', $request);
            $resp_obj->set_response(404, "failed", "sub service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check service
        $service = $this->service_model->get_service_by_id($request['service_id']);
        if(is_null($service)){
            logging('error', '/api/sub-service [PUT] - service not found', $request);
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update sub service
        $flag = $this->sub_service_model->update_sub_service($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/sub-service [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/sub-service [PUT] - Update sub service success', $request);
        $resp_obj->set_response(200, "success", "Update sub service success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/sub-service/active/$id [PUT]
    function active_sub_service($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service/active/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check sub service
        $sub_service = $this->sub_service_model->get_sub_service_by_id($id);
        if(is_null($sub_service)){
            logging('error', '/api/sub-service/active/'.$id.' [PUT] - sub service not found');
            $resp_obj->set_response(404, "failed", "sub service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update sub service
        $flag = $this->sub_service_model->active_sub_service($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/sub-service/active/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/sub-service/active/'.$id.' [PUT] - active sub service success');
        $resp_obj->set_response(200, "success", "active sub service success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/sub-service/inactive/$id [PUT]
    function inactive_sub_service($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service/inactive/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check sub service
        $sub_service = $this->sub_service_model->get_sub_service_by_id($id);
        if(is_null($sub_service)){
            logging('error', '/api/sub-service/inactive/'.$id.' [PUT] - sub service not found');
            $resp_obj->set_response(404, "failed", "sub service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update sub service
        $flag = $this->sub_service_model->inactive_sub_service($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/sub-service/inactive/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/sub-service/inactive/'.$id.' [PUT] - inactive sub service success');
        $resp_obj->set_response(200, "success", "inactive sub service success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/sub-service/delete/$id [DELETE]
    function delete_sub_service($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check sub service
        $sub_service = $this->sub_service_model->get_sub_service_by_id($id);
        if(is_null($sub_service)){
            logging('error', '/api/sub-service/delete/'.$id.' [DELETE] - sub service not found');
            $resp_obj->set_response(404, "failed", "sub service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update sub service
        $flag = $this->sub_service_model->delete_sub_service($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/sub-service/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/sub-service/delete/'.$id.' [DELETE] - Delete sub service success');
        $resp_obj->set_response(200, "success", "Delete sub service success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/sub-service/upload [POST]
    function upload_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/service/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/sub-service/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/sub-service/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data                   = $resp['data'];
        $data['full_file_url']  = base_url($data['file_url']);
        
        logging('debug', '/api/sub-service/upload [POST] - Upload image success', $data);
        $resp_obj->set_response(200, "success", "Upload image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
  
}
