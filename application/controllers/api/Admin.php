<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class Admin extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/admin [GET]
    function get_admin(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/admin [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get admin
        $start  = $page_number * $page_size;
        $order  = array('field'=>'fullname', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $admins = $this->admin_model->get_admin($search, $order, $limit);
        $total  = $this->admin_model->count_admin($search);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/admin [GET] - Get admin is success', $admin);
          $resp->set_response(200, "success", "Get admin is success", $admin);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/admin [GET] - Get admin is success');
          $resp->set_response_datatable(200, $admins, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/admin/id/$id [GET]
    function get_admin_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get admin by id
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/api/admin/id/'.$id.' [GET] - admin not found');
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }
        unset($admin->password);

        #response
        logging('debug', '/api/admin/id/'.$id.' [GET] - Get admin by id success', $admin);
        $resp->set_response(200, "success", "Get admin by id success", $admin);
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin [POST]
    function create_admin(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];
        
        #check request params
        $keys = array('username', 'fullname', 'password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/admin [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        $admin = $this->admin_model->get_admin_by_username($request['username']);
        if($admin){
            logging('error', '/api/admin [POST] - Username already registered', $request);
            $resp->set_response(400, "failed", "Username already registered");
            set_output($resp->get_response());
            return;
        }

        #create admin
        $request['id']          = get_uniq_id();
        $request['password']    = password_hash($request['password'], PASSWORD_DEFAULT);
        $flag                   = $this->admin_model->create_admin($request);
        
        #response
        if(!$flag){
            logging('error', '/api/admin [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        unset($request['password']);

        logging('debug', '/api/admin [POST] - Create admin success', $request);
        $resp->set_response(200, "success", "Create admin success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin [PUT]
    function update_admin(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'username', 'fullname');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/admin [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check admin
        $admin = $this->admin_model->get_admin_by_id($request['id']);
        if(is_null($admin)){
            logging('error', '/api/admin [PUT] - admin not found', $request);
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #check changes of username
        if($admin->username != $request['username']){
            #check duplicate username
            $admin_exist = $this->admin_model->get_admin_by_username($request['username']);
            if($admin_exist){
                logging('error', '/api/admin [POST] - New username already registered', $request);
                $resp->set_response(400, "failed", "New username already registered");
                set_output($resp->get_response());
                return;
            }
        }

        #update admin
        $flag = $this->admin_model->update_admin($request, $request['id']);
        if(empty($flag)){
            logging('error', '/api/admin [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        unset($request['password']);

        logging('debug', '/api/admin [PUT] - Update admin success', $request);
        $resp->set_response(200, "success", "Update admin success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin/inactive/$id [PUT]
    function inactive_admin($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/inactive/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check admin
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/api/admin/inactive/'.$id.' [PUT] - admin not found');
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #inactive admin
        $flag = $this->admin_model->inactive_admin($id);
        
        #response
        if(!$flag){
            logging('error', '/api/admin/inactive/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/admin/inactive/'.$id.' [PUT] - inactive admin success');
        $resp->set_response(200, "success", "inactive admin success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin/active/$id [PUT]
    function active_admin($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/active/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check admin
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/api/admin/active/'.$id.' [PUT] - admin not found');
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #active admin
        $flag = $this->admin_model->active_admin($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/admin/active/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/admin/active/'.$id.' [PUT] - active admin success');
        $resp->set_response(200, "success", "active admin success");
        set_output($resp->get_response());
        return;
    }
    
    #path: /api/admin/password [PUT]
    function change_password_admin(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];

        #check request params
        $keys = array('password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/admin/password [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #update admin
        $hash                   = password_hash($request['password'], PASSWORD_DEFAULT);
        $request['password']    = $hash;
        $request['id']          = $admin->id;
        $flag                   = $this->admin_model->change_password_admin($hash, $admin->id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/admin/password [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/admin/password [PUT] - Change password admin success', $request);
        $resp->set_response(200, "success", "Change password admin success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin/login [POST]
    function login(){
        #init req & resp
        $resp = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check request payload
        $keys = array('username', 'password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/admin/login [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #init variable
        $username   = $request['username'];
        $password   = $request['password'];

        #get admin
        $admin = $this->admin_model->get_admin_by_username($username);
        if(is_null($admin)){
            logging('error', '/api/admin/login [POST] - admin not found', $request);
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #check admin
        $verify = password_verify($password, $admin->password);
        if(!$verify){
            logging('error', '/api/admin/login [POST] - Invalid password', $request);
            $resp->set_response(400, "failed", "Invalid password");
            set_output($resp->get_response());
            return;
        }
        if(!$admin->is_active){
            logging('error', '/api/admin/login [POST] - Account is Inactive', $request);
            $resp->set_response(400, "failed", "Account is Inactive");
            set_output($resp->get_response());
            return;
        }

        #Create admin token      
        $token_expired = time() + (10 * 60);
        $payload = array(
          'username'    => $username,
          'exp'         => $token_expired
        );
        #access token
        $access_token   = JWT::encode($payload, ACCESS_TOKEN_SALT);
        #refresh token
        $payload['exp'] = time() + (60 * 60);
        $refresh_token  = JWT::encode($payload, REFRESH_TOKEN_SALT);
      
        $response = array(
          'access_token'    => $access_token,
          'refresh_token'   => $refresh_token,
          'admin'           => $admin
        );
        
        logging('debug', '/api/admin/login [POST] - admin login success', $response);
        $resp->set_response(200, "success", "admin login success", $response);
        set_output($resp->get_response());
        return;
    }

    #path: admin/refresh [GET]
    function refresh(){
      $resp = new Response_api();

      #check header
      $header = $this->input->request_headers();
      if(isset($header['Authorization'])){
        list(, $token) = explode(' ', $header['Authorization']);
      }else{
        logging('debug', '/api/admin/refresh [GET] - Please use token to access this resource.');
        $resp->set_response(401, "failed", "Please use token to access this resource.");
        set_output($resp->get_response());
        return;
      }

      try {
        $jwt = JWT::decode($token, REFRESH_TOKEN_SALT, ['HS256']);
      } catch (Exception $e) {
        logging('debug', '/api/admin/refresh [GET] - Invalid requested token');
        $resp->set_response(401, "failed", "Invalid requested token");
        set_output($resp->get_response());
        return;
      }
      $username = $jwt->username;
      $admin    = $this->admin_model->get_admin_by_username($username);

      #check admin exist
      if(is_null($admin)){
        logging('debug', '/api/admin/refresh [GET] - admin not found');
        $resp->set_response(401, "failed", "admin not found");
        set_output($resp->get_response());
        return;
      }
      if(!$admin->is_active){
        logging('error', '/api/admin/login [POST] - Admin is Inactive', $request);
        $resp->set_response(400, "failed", "Admin is Inactive");
        set_output($resp->get_response());
        return;
      }
      
      #generate new token
      $token_expired = time() + (10 * 60);
      $payload       = array(
        'username'  => $username,
        'exp'       => $token_expired
      );
      #access token
      $access_token     = JWT::encode($payload, ACCESS_TOKEN_SALT);
      #refresh token
      $payload['exp']   = time() + (60 * 60);
      $refresh_token    = JWT::encode($payload, REFRESH_TOKEN_SALT);

      $response = array(
        'access_token' => $access_token,
        'refresh_token' => $refresh_token,
        'admin' => $admin
      );
      
      logging('debug', '/api/admin/refresh [GET] - Refresh admin success', $response);
      $resp->set_response(200, "success", "Refresh admin success", $response);
      set_output($resp->get_response());
      return;
    }

    #path: /api/admin/profile [GET]
    function get_profile(){
        $resp = new Response_api();
  
        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/profile [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];
        unset($admin->password);
  
        #response
        logging('debug', '/api/admin/profile [GET] - Get profile success', $admin);
        $resp->set_response(200, "success", "Get profile success", $admin);
        set_output($resp->get_response());
        return;
    }
}