<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Message extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/message/total [GET]
    function get_message_total(){
        #init req & resp
        $resp_obj   = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get message total
        $total = $this->message_model->count_message(null, 0);

        #response
        $resp_obj->set_response(200, "success", "Get message total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/message [GET]
    function get_message(){
        #init req & resp
        $resp_obj   = new Response_api();
        $page_number= $this->input->get('page_number');
        $page_size  = $this->input->get('page_size');
        $search     = $this->input->get('search');
        $is_read    = $this->input->get('is_read');
        $draw       = $this->input->get('draw');
        $params     = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/message [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get message
        $start      = $page_number * $page_size;
        $limit      = array('start'=>$start, 'size'=>$page_size);
        $order      = array('field'=>'created_at', 'order'=>'DESC');
        $message    = $this->message_model->get_message($search, $is_read, $order, $limit);
        $total      = $this->message_model->count_message($search, $is_read);

        #response
        if(empty($draw)){
            $resp_obj->set_response(200, "success", "Get message is success", $message);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/api/message [GET] - Get message is success', $message);
            $resp_obj->set_response_datatable(200, $message, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /api/message/id/$id [GET]
    function get_message_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/message/id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get message detail
        $message = $this->message_model->get_message_by_id($id);
        if(is_null($message)){
            logging('error', '/api/message/id/'.$id.' [GET] - message not found');
            $resp_obj->set_response(404, "failed", "message not found");
            set_output($resp_obj->get_response());
            return;
        }

        if($message->product_id){
            $product_image = $this->product_image_model->get_product_image_by_product_id($message->product_id);
            $message->product_image = $product_image[0]->img_url;
        }

        #response
        logging('debug', '/api/message/id/'.$id.' [GET] - Get message by id success', $message);
        $resp_obj->set_response(200, "success", "Get message by id success", $message);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/message [POST]
    function create_message(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        // $header = $this->input->request_headers();
        // $resp   = verify_admin_token($header);
        // if($resp['status'] == 'failed'){
        //     logging('error', '/api/message [POST] - '.$resp['message']);
        //     set_output($resp);
        //     return;
        // }
        
        #check request params
        $keys       = array('fullname', 'phone', 'email', 'company', 'content', 'has_source');
        $check_res  = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', "/api/message [POST] - ".$check_res['message'], $request);
            $resp_obj->set_response(400, "failed", $check_res['message']);
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']  = get_uniq_id();
        $flag           = $this->message_model->create_message($request);
        
        #response
        if(!$flag){
            logging('error', '/api/message [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }

        #send mail
        $config = $this->config_model->get_config();
        if($config){
            if($config->email){
                $product = null;
                $service = null;
                if(intval($request['has_source'])){
                    if(array_key_exists("product_id", $request)){
                        $product = $this->product_model->get_product_by_id($request['product_id']);
                    }
                    if(array_key_exists("service_id", $request)){
                        $service = $this->service_model->get_service_by_id($request['service_id']);
                    }
                }

                $emails = explode(",", $config->email);
                foreach ($emails as $email) {
                    send_order_email($email, $request, $product, $service);
                }
            }
        }

        logging('debug', '/api/message [POST] - Create message success', $request);
        $resp_obj->set_response(200, "success", "Create message success", $request);
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /api/message/read/$id [POST]
    function read_message($id){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/message/read/$id [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #read message
        $flag = $this->message_model->read_message($id);
        
        #response
        if(!$flag){
            logging('error', '/api/message/read/$1 [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/message/read/$1 [POST] - Read message success', $request);
        $resp_obj->set_response(200, "success", "Read message success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/message/read-all [POST]
    function read_message_all(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/message/read-all [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #read message
        $flag = $this->message_model->read_message_all();
        
        #response
        if(!$flag){
            logging('error', '/api/message/read-all [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/message/read-all [POST] - Read message all success', $request);
        $resp_obj->set_response(200, "success", "Read message all success", $request);
        set_output($resp_obj->get_response());
        return;
    }
}