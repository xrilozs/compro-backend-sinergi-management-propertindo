<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Product_image extends CI_Controller {
    #path: /api/product/image/$1 [GET]
    function get_product_image_by_product($id){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/image/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $product_images = $this->product_image_model->get_product_image_by_product_id($id);

        logging('debug', '/api/product/image/$id [GET] - Get product image by product success');
        $resp_obj->set_response(200, "success", "Get product image by product success", $product_images);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/image [POST]
    function add_product_image(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/image [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('product_id', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/product/image [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }


        #create photo
        $request['id']  = get_uniq_id();
        $flag           = $this->product_image_model->create_product_image($request);
        
        #response
        if(!$flag){
            logging('error', '/api/product/image [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product/image [POST] - Add product image success', $request);
        $resp_obj->set_response(200, "success", "Add product image success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/image/delete/$id [DELETE]
    function remove_product_image($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/image/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check product image
        $product_image = $this->product_image_model->get_product_image_by_id($id);
        if(is_null($product_image)){
            logging('error', '/api/product/image/delete/'.$id.' [DELETE] - product image not found');
            $resp_obj->set_response(404, "failed", "product image not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete product image
        $flag = $this->product_image_model->delete_product_image($id);
        if(empty($flag)){
            logging('error', '/api/product/image/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product/image/delete/'.$id.' [DELETE] - Delete product image success');
        $resp_obj->set_response(200, "success", "Delete product image success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/image/upload [POST]
    function upload_product_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/image/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/product/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/product/image/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/image/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data                   = $resp['data'];
        $data['full_file_url']  = base_url($data['file_url']);
        
        logging('debug', '/api/product/image/upload [POST] - Upload product image success', $data);
        $resp_obj->set_response(200, "success", "Upload product image success", $data);
        set_output($resp_obj->get_response());
        return;
    }
}
?>