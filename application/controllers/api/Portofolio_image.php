<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Portofolio_image extends CI_Controller {
    #path: /api/portofolio/image/$1 [GET]
    function get_portofolio_image_by_portofolio($id){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/image/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $portofolio_images = $this->portofolio_image_model->get_portofolio_image_by_portofolio_id($id);

        logging('debug', '/api/portofolio/image/$id [GET] - Get portofolio image by portofolio success');
        $resp_obj->set_response(200, "success", "Get portofolio image by portofolio success", $portofolio_images);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/image [POST]
    function add_portofolio_image(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/image [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('portofolio_id', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/portofolio/image [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }


        #create photo
        $request['id']  = get_uniq_id();
        $flag           = $this->portofolio_image_model->create_portofolio_image($request);
        
        #response
        if(!$flag){
            logging('error', '/api/portofolio/image [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/portofolio/image [POST] - Add portofolio image success', $request);
        $resp_obj->set_response(200, "success", "Add portofolio image success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/image/delete/$id [DELETE]
    function remove_portofolio_image($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/image/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check portofolio image
        $portofolio_image = $this->portofolio_image_model->get_portofolio_image_by_id($id);
        if(is_null($portofolio_image)){
            logging('error', '/api/portofolio/image/delete/'.$id.' [DELETE] - portofolio image not found');
            $resp_obj->set_response(404, "failed", "portofolio image not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete portofolio image
        $flag = $this->portofolio_image_model->delete_portofolio_image($id);
        if(empty($flag)){
            logging('error', '/api/portofolio/image/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/portofolio/image/delete/'.$id.' [DELETE] - Delete portofolio image success');
        $resp_obj->set_response(200, "success", "Delete portofolio image success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/image/upload [POST]
    function upload_portofolio_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/image/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/portofolio/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/portofolio/image/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/image/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data                   = $resp['data'];
        $data['full_file_url']  = base_url($data['file_url']);
        
        logging('debug', '/api/portofolio/image/upload [POST] - Upload portofolio image success', $data);
        $resp_obj->set_response(200, "success", "Upload portofolio image success", $data);
        set_output($resp_obj->get_response());
        return;
    }
}
?>