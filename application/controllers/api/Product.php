<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Product extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/product/total [GET]
    function get_product_total(){
        #init req & resp
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/total [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get product total
        $total = $this->product_model->count_product(null, 1);

        logging('debug', '/api/product [GET] - Get product total is success', $total);
        $resp_obj->set_response(200, "success", "Get product total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product [GET]
    function get_product(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $is_active      = $this->input->get('is_active');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/api/product [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get product
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $product     = $this->product_model->get_product($search, $is_active, $order, $limit);
        $total          = $this->product_model->count_product($search, $is_active);

        if(empty($draw)){
            logging('debug', '/api/product [GET] - Get product is success', $product);
            $resp_obj->set_response(200, "success", "Get product is success", $product);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/api/product [GET] - Get product is success', $output);
            $resp_obj->set_response_datatable(200, $product, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/product/id/$id [GET]
    function get_product_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get product detail
        $product = $this->product_model->get_product_by_id($id);
        if(is_null($product)){
            logging('error', '/api/product/id/'.$id.' [GET] - product not found');
            $resp_obj->set_response(404, "failed", "product not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/product/id/'.$id.' [GET] - Get product by id success', $product);
        $resp_obj->set_response(200, "success", "Get product by id success", $product);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/alias/$alias [GET]
    function get_product_by_alias($alias){
        $resp_obj = new Response_api();

        #get product detail
        $product = $this->product_model->get_product_by_alias($alias);
        if(is_null($product)){
            logging('error', '/api/product/alias/'.$alias.' [GET] - product not found');
            $resp_obj->set_response(404, "failed", "product not found");
            set_output($resp_obj->get_response());
            return;
        }

        $product_images = $this->product_image_model->get_product_image_by_product_id($product->id);
        $product->image = $product_images;

        #response
        logging('debug', '/api/product/alias/'.$alias.' [GET] - Get product by alias success', $product);
        $resp_obj->set_response(200, "success", "Get product by alias success", $product);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/category/$category_id [GET]
    function get_product_by_category($category_id){
        $resp_obj = new Response_api();

        #get product detail
        $products = $this->product_model->get_product_by_category_id($category_id);

        foreach ($products as &$product) {
            $product_images = $this->product_image_model->get_product_image_by_product_id($product->id);
            $product->image = $product_images;
        }

        #response
        logging('debug', '/api/product/category/'.$category_id.' [GET] - Get product by category success');
        $resp_obj->set_response(200, "success", "Get product by category success", $products);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product [POST]
    function create_product(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('category_id', 'name', 'description');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/product [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']      = get_uniq_id();
        $request['alias']   = generate_alias($request['name']);
    
        #check product exist
        $product_exist = $this->product_model->get_product_by_alias($request['alias']);
        if($product_exist){
            logging('error', '/api/product [POST] - Requested product already exist', $request);
            $resp_obj->set_response(400, "failed", "Requested product already exist");
            set_output($resp_obj->get_response());
            return;
        }

        #create product
        $flag = $this->product_model->create_product($request);
        
        #response
        if(!$flag){
            logging('error', '/api/product [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product [POST] - Create product success', $request);
        $resp_obj->set_response(200, "success", "Create product success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product [PUT]
    function update_product(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'category_id', 'name', 'description');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/product [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check product
        $product = $this->product_model->get_product_by_id($request['id']);
        if(is_null($product)){
            logging('error', '/api/product [PUT] - product not found', $request);
            $resp_obj->set_response(404, "failed", "product not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product
        $request['alias']       = generate_alias($request['name']);
        $request['is_active']   = $product->is_active;
        if($product->alias != $request['alias']){
            $product_exist = $this->product_model->get_product_by_alias($request['alias']);
            if($product_exist){
                logging('error', '/api/product [PUT] - product title already exist', $request);
                $resp_obj->set_response(400, "failed", "product title already exist");
                set_output($resp_obj->get_response());
                return;
            }
        }

        $flag = $this->product_model->update_product($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product [PUT] - Update product success', $request);
        $resp_obj->set_response(200, "success", "Update product success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/active/$id [PUT]
    function active_product($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/active/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check product
        $product = $this->product_model->get_product_by_id($id);
        if(is_null($product)){
            logging('error', '/api/product/active/'.$id.' [PUT] - product not found');
            $resp_obj->set_response(404, "failed", "product not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product
        $flag = $this->product_model->active_product($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product/active/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product/active/'.$id.' [PUT] - active product success');
        $resp_obj->set_response(200, "success", "active product success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/inactive/$id [PUT]
    function inactive_product($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/inactive/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check product
        $product = $this->product_model->get_product_by_id($id);
        if(is_null($product)){
            logging('error', '/api/product/inactive/'.$id.' [PUT] - product not found');
            $resp_obj->set_response(404, "failed", "product not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product
        $flag = $this->product_model->inactive_product($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product/inactive/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product/inactive/'.$id.' [PUT] - inactive product success');
        $resp_obj->set_response(200, "success", "inactive product success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product/delete/$id [DELETE]
    function delete_product($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check product
        $product = $this->product_model->get_product_by_id($id);
        if(is_null($product)){
            logging('error', '/api/product/delete/'.$id.' [DELETE] - product not found');
            $resp_obj->set_response(404, "failed", "product not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product
        $flag = $this->product_model->delete_product($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product/delete/'.$id.' [DELETE] - Delete product success');
        $resp_obj->set_response(200, "success", "Delete product success");
        set_output($resp_obj->get_response());
        return;
    }
  
}
