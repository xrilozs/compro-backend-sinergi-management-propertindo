<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Custom_page extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/custom-page [GET]
    function get_custom_page(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $is_active      = $this->input->get('is_active');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/custom-page [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/api/custom-page [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get custom page
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $custom_page    = $this->custom_page_model->get_custom_page($search, $is_active, $order, $limit);
        $total          = $this->custom_page_model->count_custom_page($search, $is_active);

        if(empty($draw)){
            logging('debug', '/api/custom-page [GET] - Get custom page is success', $custom_page);
            $resp_obj->set_response(200, "success", "Get custom page is success", $custom_page);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/api/custom-page [GET] - Get custom page is success', $output);
            $resp_obj->set_response_datatable(200, $custom_page, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/custom-page/position [GET]
    function get_custom_page_by_position(){
        $resp_obj = new Response_api();
        $position = $this->input->get('position');
        $list_position = explode(",", $position);

        #get custom page
        $custom_page = $this->custom_page_model->get_custom_page_by_position($list_position);

        #response
        logging('debug', '/api/custom-page/position [GET] - Get custom page by position success', $custom_page);
        $resp_obj->set_response(200, "success", "Get custom page by position success", $custom_page);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/custom-page/id/$id [GET]
    function get_custom_page_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/custom-page/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get custom page detail
        $custom_page = $this->custom_page_model->get_custom_page_by_id($id);
        if(is_null($custom_page)){
            logging('error', '/api/custom-page/id/'.$id.' [GET] - custom page not found');
            $resp_obj->set_response(404, "failed", "custom_page not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/custom-page/id/'.$id.' [GET] - Get custom page by id success', $custom_page);
        $resp_obj->set_response(200, "success", "Get custom page by id success", $custom_page);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/custom-page/alias/$alias [GET]
    function get_custom_page_by_alias($alias){
        $resp_obj = new Response_api();

        #get custom page detail
        $custom_page = $this->custom_page_model->get_custom_page_by_alias($alias);
        if(is_null($custom_page)){
            logging('error', '/api/custom-page/alias/'.$alias.' [GET] - custom page not found');
            $resp_obj->set_response(404, "failed", "custom_page not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/custom-page/alias/'.$alias.' [GET] - Get custom page by alias success', $custom_page);
        $resp_obj->set_response(200, "success", "Get custom page by alias success", $custom_page);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/custom-page [POST]
    function create_custom_page(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/custom-page [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('title', 'content', 'position');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/custom-page [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']      = get_uniq_id();
        $request['alias']   = generate_alias($request['title']);
    
        #check custom page exist
        $custom_page_exist = $this->custom_page_model->get_custom_page_by_alias($request['alias']);
        if($custom_page_exist){
            logging('error', '/api/custom-page [POST] - Requested custom page already exist', $request);
            $resp_obj->set_response(400, "failed", "Requested custom page already exist");
            set_output($resp_obj->get_response());
            return;
        }

        #create custom page
        $flag = $this->custom_page_model->create_custom_page($request);
        
        #response
        if(!$flag){
            logging('error', '/api/custom-page [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/custom-page [POST] - Create custom page success', $request);
        $resp_obj->set_response(200, "success", "Create custom page success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/custom-page [PUT]
    function update_custom_page(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/custom-page [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'title', 'content', 'position');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/custom-page [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check custom page
        $custom_page = $this->custom_page_model->get_custom_page_by_id($request['id']);
        if(is_null($custom_page)){
            logging('error', '/api/custom-page [PUT] - custom page not found', $request);
            $resp_obj->set_response(404, "failed", "custom page not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update custom page
        $request['alias']       = generate_alias($request['title']);
        $request['is_active']   = $custom_page->is_active;
        if($custom_page->alias != $request['alias']){
            $custom_page_exist = $this->custom_page_model->get_custom_page_by_alias($request['alias']);
            if($custom_page_exist){
                logging('error', '/api/custom-page [PUT] - custom page title already exist', $request);
                $resp_obj->set_response(400, "failed", "custom page title already exist");
                set_output($resp_obj->get_response());
                return;
            }
        }

        $flag = $this->custom_page_model->update_custom_page($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/custom-page [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/custom-page [PUT] - Update custom page success', $request);
        $resp_obj->set_response(200, "success", "Update custom page success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/custom-page/active/$id [PUT]
    function active_custom_page($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/custom-page/active/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check custom page
        $custom_page = $this->custom_page_model->get_custom_page_by_id($id);
        if(is_null($custom_page)){
            logging('error', '/api/custom-page/active/'.$id.' [PUT] - custom page not found');
            $resp_obj->set_response(404, "failed", "custom page not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update custom page
        $flag = $this->custom_page_model->active_custom_page($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/custom-page/active/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/custom-page/active/'.$id.' [PUT] - active custom page success');
        $resp_obj->set_response(200, "success", "active custom page success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/custom-page/inactive/$id [PUT]
    function inactive_custom_page($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/custom-page/inactive/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check custom page
        $custom_page = $this->custom_page_model->get_custom_page_by_id($id);
        if(is_null($custom_page)){
            logging('error', '/api/custom-page/inactive/'.$id.' [PUT] - custom page not found');
            $resp_obj->set_response(404, "failed", "custom page not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update custom page
        $flag = $this->custom_page_model->inactive_custom_page($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/custom-page/inactive/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/custom-page/inactive/'.$id.' [PUT] - inactive custom page success');
        $resp_obj->set_response(200, "success", "inactive custom page success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/custom-page/$id [DELETE]
    function delete_custom_page($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/custom-page/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check custom page
        $custom_page = $this->custom_page_model->get_custom_page_by_id($id);
        if(is_null($custom_page)){
            logging('error', '/api/custom-page/'.$id.' [DELETE] - custom page not found');
            $resp_obj->set_response(404, "failed", "custom page not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update custom page
        $flag = $this->custom_page_model->delete_custom_page($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/custom-page/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/custom-page/'.$id.' [DELETE] - Delete custom page success');
        $resp_obj->set_response(200, "success", "Delete custom page success");
        set_output($resp_obj->get_response());
        return;
    }
  
}
