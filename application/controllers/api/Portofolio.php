<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Portofolio extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/portofolio/total [GET]
    function get_portofolio_total(){
        #init req & resp
        $resp_obj       = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/total [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get portofolio total
        $total = $this->portofolio_model->count_portofolio(null, 1);

        logging('debug', '/api/portofolio [GET] - Get portofolio total is success', $total);
        $resp_obj->set_response(200, "success", "Get portofolio total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/all [GET]
    function get_portofolio_all(){
        #init req & resp
        $resp_obj       = new Response_api();
        $category       = $this->input->get('category');

        #get portofolio
        $portofolio = $this->portofolio_model->get_portofolio(null, $category, 1);

        foreach ($portofolio as &$item) {
            $images       = $this->portofolio_image_model->get_portofolio_image_by_portofolio_id($item->id);
            $item->image  = $images;
        }

        logging('debug', '/api/portofolio [GET] - Get portofolio all is success', $portofolio);
        $resp_obj->set_response(200, "success", "Get portofolio all is success", $portofolio);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio [GET]
    function get_portofolio(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $is_active      = $this->input->get('is_active');
        $category       = $this->input->get('category');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/api/portofolio [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get portofolio
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $portofolio     = $this->portofolio_model->get_portofolio($search, $category, $is_active, $order, $limit);
        $total          = $this->portofolio_model->count_portofolio($search, $category, $is_active);

        if(empty($draw)){
            logging('debug', '/api/portofolio [GET] - Get portofolio is success', $portofolio);
            $resp_obj->set_response(200, "success", "Get portofolio is success", $portofolio);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/api/portofolio [GET] - Get portofolio is success', $output);
            $resp_obj->set_response_datatable(200, $portofolio, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/portofolio/id/$id [GET]
    function get_portofolio_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get portofolio detail
        $portofolio = $this->portofolio_model->get_portofolio_by_id($id);
        if(is_null($portofolio)){
            logging('error', '/api/portofolio/id/'.$id.' [GET] - portofolio not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/portofolio/id/'.$id.' [GET] - Get portofolio by id success', $portofolio);
        $resp_obj->set_response(200, "success", "Get portofolio by id success", $portofolio);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/alias/$alias [GET]
    function get_portofolio_by_alias($alias){
        $resp_obj = new Response_api();

        #get portofolio detail
        $portofolio = $this->portofolio_model->get_portofolio_by_alias($alias);
        if(is_null($portofolio)){
            logging('error', '/api/portofolio/alias/'.$alias.' [GET] - portofolio not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        $images             = $this->portofolio_image_model->get_portofolio_image_by_portofolio_id($portofolio->id);
        $portofolio->image  = $images;

        #response
        logging('debug', '/api/portofolio/alias/'.$alias.' [GET] - Get portofolio by alias success', $portofolio);
        $resp_obj->set_response(200, "success", "Get portofolio by alias success", $portofolio);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio [POST]
    function create_portofolio(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('title', 'category', 'content', 'date', 'client');
        $check_res = check_parameter_by_keysV2($request, $keys);
        if(!$check_res['success']){
            logging('error', '/api/portofolio [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", $check_res['message']);
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']      = get_uniq_id();
        $request['alias']   = generate_alias($request['title']);
    
        #check portofolio exist
        $portofolio_exist = $this->portofolio_model->get_portofolio_by_alias($request['alias']);
        if($portofolio_exist){
            logging('error', '/api/portofolio [POST] - Requested portofolio already exist', $request);
            $resp_obj->set_response(400, "failed", "Requested portofolio already exist");
            set_output($resp_obj->get_response());
            return;
        }

        #create portofolio
        $flag = $this->portofolio_model->create_portofolio($request);
        
        #response
        if(!$flag){
            logging('error', '/api/portofolio [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/portofolio [POST] - Create portofolio success', $request);
        $resp_obj->set_response(200, "success", "Create portofolio success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio [PUT]
    function update_portofolio(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'title', 'category', 'content', 'date', 'client');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/portofolio [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check portofolio
        $portofolio = $this->portofolio_model->get_portofolio_by_id($request['id']);
        if(is_null($portofolio)){
            logging('error', '/api/portofolio [PUT] - portofolio not found', $request);
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update portofolio
        $request['alias']       = generate_alias($request['title']);
        $request['is_active']   = $portofolio->is_active;
        if($portofolio->alias != $request['alias']){
            $portofolio_exist = $this->portofolio_model->get_portofolio_by_alias($request['alias']);
            if($portofolio_exist){
                logging('error', '/api/portofolio [PUT] - portofolio title already exist', $request);
                $resp_obj->set_response(400, "failed", "portofolio title already exist");
                set_output($resp_obj->get_response());
                return;
            }
        }

        $flag = $this->portofolio_model->update_portofolio($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/portofolio [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/portofolio [PUT] - Update portofolio success', $request);
        $resp_obj->set_response(200, "success", "Update portofolio success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/active/$id [PUT]
    function active_portofolio($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/active/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check portofolio
        $portofolio = $this->portofolio_model->get_portofolio_by_id($id);
        if(is_null($portofolio)){
            logging('error', '/api/portofolio/active/'.$id.' [PUT] - portofolio not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update portofolio
        $flag = $this->portofolio_model->active_portofolio($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/portofolio/active/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/portofolio/active/'.$id.' [PUT] - active portofolio success');
        $resp_obj->set_response(200, "success", "active portofolio success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/inactive/$id [PUT]
    function inactive_portofolio($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/inactive/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check portofolio
        $portofolio = $this->portofolio_model->get_portofolio_by_id($id);
        if(is_null($portofolio)){
            logging('error', '/api/portofolio/inactive/'.$id.' [PUT] - portofolio not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update portofolio
        $flag = $this->portofolio_model->inactive_portofolio($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/portofolio/inactive/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/portofolio/inactive/'.$id.' [PUT] - inactive portofolio success');
        $resp_obj->set_response(200, "success", "inactive portofolio success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/portofolio/delete/$id [DELETE]
    function delete_portofolio($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/portofolio/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check portofolio
        $portofolio = $this->portofolio_model->get_portofolio_by_id($id);
        if(is_null($portofolio)){
            logging('error', '/api/portofolio/delete/'.$id.' [DELETE] - portofolio not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update portofolio
        $flag = $this->portofolio_model->delete_portofolio($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/portofolio/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/portofolio/delete/'.$id.' [DELETE] - Delete portofolio success');
        $resp_obj->set_response(200, "success", "Delete portofolio success");
        set_output($resp_obj->get_response());
        return;
    }
  
}
