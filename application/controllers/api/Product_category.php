<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Product_category extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/product-category/all [GET]
    function get_product_category_all(){
        #init req & resp
        $resp_obj = new Response_api();

        #get product category all
        $product_category = $this->product_category_model->get_product_category(null, 1);

        logging('debug', '/api/product-category [GET] - Get product category all is success', $product_category);
        $resp_obj->set_response(200, "success", "Get product category all is success", $product_category);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category/total [GET]
    function get_product_category_total(){
        #init req & resp
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category/total [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get product category total
        $total = $this->product_category_model->count_product_category(null, 1);

        logging('debug', '/api/product-category/total [GET] - Get product category total is success', $total);
        $resp_obj->set_response(200, "success", "Get product category total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category [GET]
    function get_product_category(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $is_active      = $this->input->get('is_active');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/api/product-category [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get product category
        $start              = $page_number * $page_size;
        $order              = array('field'=>'created_at', 'order'=>'DESC');
        $limit              = array('start'=>$start, 'size'=>$page_size);
        $product_category   = $this->product_category_model->get_product_category($search, $is_active, $order, $limit);
        $total              = $this->product_category_model->count_product_category($search, $is_active);

        if(empty($draw)){
            logging('debug', '/api/product-category [GET] - Get product category is success', $product_category);
            $resp_obj->set_response(200, "success", "Get product category is success", $product_category);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/product-category [GET] - Get product category is success');
            $resp_obj->set_response_datatable(200, $product_category, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/product-category/id/$id [GET]
    function get_product_category_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get product category detail
        $product_category = $this->product_category_model->get_product_category_by_id($id);
        if(is_null($product_category)){
            logging('error', '/api/product-category/id/'.$id.' [GET] - product category not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/product-category/id/'.$id.' [GET] - Get product category by id success', $product_category);
        $resp_obj->set_response(200, "success", "Get product category by id success", $product_category);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category/alias/$alias [GET]
    function get_product_category_by_alias($alias){
        $resp_obj = new Response_api();

        #check token
        // $header = $this->input->request_headers();
        // $resp   = verify_admin_token($header);
        // if($resp['status'] == 'failed'){
        //     logging('error', '/api/product-category/alias/'.$alias.' [GET] - '.$resp['message']);
        //     set_output($resp);
        //     return;
        // }

        #get product category detail
        $product_category = $this->product_category_model->get_product_category_by_alias($alias);
        if(is_null($product_category)){
            logging('error', '/api/product-category/alias/'.$alias.' [GET] - product category not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/product-category/alias/'.$alias.' [GET] - Get product category by alias success', $product_category);
        $resp_obj->set_response(200, "success", "Get product category by alias success", $product_category);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category [POST]
    function create_product_category(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/product-category [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']      = get_uniq_id();
        $request['alias']   = generate_alias($request['name']);
    
        #check product category exist
        $product_category_exist = $this->product_category_model->get_product_category_by_alias($request['alias']);
        if($product_category_exist){
            logging('error', '/api/product-category [POST] - Requested product category already exist', $request);
            $resp_obj->set_response(400, "failed", "Requested product category already exist");
            set_output($resp_obj->get_response());
            return;
        }

        #create product category
        $flag = $this->product_category_model->create_product_category($request);
        
        #response
        if(!$flag){
            logging('error', '/api/product-category [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product-category [POST] - Create product category success', $request);
        $resp_obj->set_response(200, "success", "Create product category success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category [PUT]
    function update_product_category(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/product-category [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check product category
        $product_category = $this->product_category_model->get_product_category_by_id($request['id']);
        if(is_null($product_category)){
            logging('error', '/api/product-category [PUT] - product category not found', $request);
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product category
        $request['alias']       = generate_alias($request['name']);
        $request['is_active']   = $product_category->is_active;
        if($product_category->alias != $request['alias']){
            $product_category_exist = $this->product_category_model->get_product_category_by_alias($request['alias']);
            if($product_category_exist){
                logging('error', '/api/product-category [PUT] - product category title already exist', $request);
                $resp_obj->set_response(400, "failed", "portofolio title already exist");
                set_output($resp_obj->get_response());
                return;
            }
        }

        $flag = $this->product_category_model->update_product_category($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product-category [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product-category [PUT] - Update product category success', $request);
        $resp_obj->set_response(200, "success", "Update product category success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category/active/$id [PUT]
    function active_product_category($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category/active/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check product category
        $product_category = $this->product_category_model->get_product_category_by_id($id);
        if(is_null($product_category)){
            logging('error', '/api/product-category/active/'.$id.' [PUT] - product category not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product category
        $flag = $this->product_category_model->active_product_category($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product-category/active/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product-category/active/'.$id.' [PUT] - active product category success');
        $resp_obj->set_response(200, "success", "active product category success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category/inactive/$id [PUT]
    function inactive_product_category($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category/inactive/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check product category
        $product_category = $this->product_category_model->get_product_category_by_id($id);
        if(is_null($product_category)){
            logging('error', '/api/product-category/inactive/'.$id.' [PUT] - product category not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product category
        $flag = $this->product_category_model->inactive_product_category($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product-category/inactive/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product-category/inactive/'.$id.' [PUT] - inactive product category success');
        $resp_obj->set_response(200, "success", "inactive product category success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category/delete/$id [DELETE]
    function delete_product_category($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check product category
        $product_category = $this->product_category_model->get_product_category_by_id($id);
        if(is_null($product_category)){
            logging('error', '/api/product-category/delete/'.$id.' [DELETE] - product category not found');
            $resp_obj->set_response(404, "failed", "portofolio not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update product category
        $flag = $this->product_category_model->delete_product_category($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/product-category/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/product-category/delete/'.$id.' [DELETE] - Delete product category success');
        $resp_obj->set_response(200, "success", "Delete product category success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/product-category/upload [POST]
    function upload_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/category/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/product-category/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/product-category/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data                   = $resp['data'];
        $data['full_file_url']  = base_url($data['file_url']);
        
        logging('debug', '/api/product-category/upload [POST] - Upload image success', $data);
        $resp_obj->set_response(200, "success", "Upload image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
  
}
