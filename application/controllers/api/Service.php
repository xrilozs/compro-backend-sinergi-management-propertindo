<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Service extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/service/total [GET]
    function get_service_total(){
        #init req & resp
        $resp_obj       = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service/total [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get service total
        $total = $this->service_model->count_service(null, 1);

        logging('debug', '/api/service [GET] - Get service total is success', $total);
        $resp_obj->set_response(200, "success", "Get service total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service/all [GET]
    function get_service_all(){
        #init req & resp
        $resp_obj       = new Response_api();

        #get service
        $order      = array('field'=>'created_at', 'order'=>'DESC');
        $service    = $this->service_model->get_service(null, 1, $order);

        logging('debug', '/api/service [GET] - Get service is success', $service);
        $resp_obj->set_response(200, "success", "Get service is success", $service);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service [GET]
    function get_service(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $is_active      = $this->input->get('is_active');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/api/service [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get service
        $start      = $page_number * $page_size;
        $order      = array('field'=>'created_at', 'order'=>'DESC');
        $limit      = array('start'=>$start, 'size'=>$page_size);
        $service    = $this->service_model->get_service($search, $is_active, $order, $limit);
        $total      = $this->service_model->count_service($search, $is_active);

        if(empty($draw)){
            logging('debug', '/api/service [GET] - Get service is success', $service);
            $resp_obj->set_response(200, "success", "Get service is success", $service);
            set_output($resp_obj->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/api/service [GET] - Get service is success', $output);
            $resp_obj->set_response_datatable(200, $service, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/service/id/$id [GET]
    function get_service_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get service detail
        $service = $this->service_model->get_service_by_id($id);
        if(is_null($service)){
            logging('error', '/api/service/id/'.$id.' [GET] - service not found');
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/service/id/'.$id.' [GET] - Get service by id success', $service);
        $resp_obj->set_response(200, "success", "Get service by id success", $service);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service/alias/$alias [GET]
    function get_service_by_alias($alias){
        $resp_obj = new Response_api();

        #get service detail
        $service = $this->service_model->get_service_by_alias($alias);
        if(is_null($service)){
            logging('error', '/api/service/alias/'.$alias.' [GET] - service not found');
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        $sub_service            = $this->sub_service_model->get_sub_service(null, $service->id);
        $service->sub_service   = $sub_service;

        #response
        logging('debug', '/api/service/alias/'.$alias.' [GET] - Get service by alias success', $service);
        $resp_obj->set_response(200, "success", "Get service by alias success", $service);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service [POST]
    function create_service(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'description');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/service [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']      = get_uniq_id();
        $request['alias']   = generate_alias($request['name']);
    
        #check service exist
        $service_exist = $this->service_model->get_service_by_alias($request['alias']);
        if($service_exist){
            logging('error', '/api/service [POST] - Requested service already exist', $request);
            $resp_obj->set_response(400, "failed", "Requested service already exist");
            set_output($resp_obj->get_response());
            return;
        }

        #create service
        $flag = $this->service_model->create_service($request);
        
        #response
        if(!$flag){
            logging('error', '/api/service [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/service [POST] - Create service success', $request);
        $resp_obj->set_response(200, "success", "Create service success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service [PUT]
    function update_service(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'description');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/service [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check service
        $service = $this->service_model->get_service_by_id($request['id']);
        if(is_null($service)){
            logging('error', '/api/service [PUT] - service not found', $request);
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update service
        $request['alias']       = generate_alias($request['name']);
        $request['is_active']   = $service->is_active;
        if($service->alias != $request['alias']){
            $service_exist = $this->service_model->get_service_by_alias($request['alias']);
            if($service_exist){
                logging('error', '/api/service [PUT] - service title already exist', $request);
                $resp_obj->set_response(400, "failed", "service title already exist");
                set_output($resp_obj->get_response());
                return;
            }
        }

        $flag = $this->service_model->update_service($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/service [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/service [PUT] - Update service success', $request);
        $resp_obj->set_response(200, "success", "Update service success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service/active/$id [PUT]
    function active_service($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service/active/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check service
        $service = $this->service_model->get_service_by_id($id);
        if(is_null($service)){
            logging('error', '/api/service/active/'.$id.' [PUT] - service not found');
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update service
        $flag = $this->service_model->active_service($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/service/active/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/service/active/'.$id.' [PUT] - active service success');
        $resp_obj->set_response(200, "success", "active service success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service/inactive/$id [PUT]
    function inactive_service($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service/inactive/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check service
        $service = $this->service_model->get_service_by_id($id);
        if(is_null($service)){
            logging('error', '/api/service/inactive/'.$id.' [PUT] - service not found');
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update service
        $flag = $this->service_model->inactive_service($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/service/inactive/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/service/inactive/'.$id.' [PUT] - inactive service success');
        $resp_obj->set_response(200, "success", "inactive service success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/service/delete/$id [DELETE]
    function delete_service($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/service/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check service
        $service = $this->service_model->get_service_by_id($id);
        if(is_null($service)){
            logging('error', '/api/service/delete/'.$id.' [DELETE] - service not found');
            $resp_obj->set_response(404, "failed", "service not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update service
        $flag = $this->service_model->delete_service($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/service/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/service/delete/'.$id.' [DELETE] - Delete service success');
        $resp_obj->set_response(200, "success", "Delete service success");
        set_output($resp_obj->get_response());
        return;
    }
  
}
