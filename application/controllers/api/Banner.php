<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Banner extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/banner/all [GET]
    function get_banner_all(){
        #init req & resp
        $resp_obj       = new Response_api();

        #get banner
        $order  = array('field'=>'order_no', 'order'=>'DESC');
        $banner = $this->banner_model->get_banner(null, $order);

        logging('debug', '/api/banner [GET] - Get banner is success', $banner);
        $resp_obj->set_response(200, "success", "Get banner is success", $banner);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/banner [GET]
    function get_banner(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/banner [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/api/banner [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get banner
        $start  = $page_number * $page_size;
        $order  = array('field'=>'order_no', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $banner = $this->banner_model->get_banner($search, $order, $limit);
        $total  = $this->banner_model->count_banner($search);

        if(empty($draw)){
            logging('debug', '/api/banner [GET] - Get banner is success', $banner);
            $resp_obj->set_response(200, "success", "Get banner is success", $banner);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/banner [GET] - Get banner is success');
            $resp_obj->set_response_datatable(200, $banner, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/banner/id/$id [GET]
    function get_banner_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/banner/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get banner detail
        $banner = $this->banner_model->get_banner_by_id($id);
        if(is_null($banner)){
            logging('error', '/api/banner/id/'.$id.' [GET] - banner not found');
            $resp_obj->set_response(404, "failed", "banner not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/banner/id/'.$id.' [GET] - Get banner by id success', $banner);
        $resp_obj->set_response(200, "success", "Get banner by id success", $banner);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/banner [POST]
    function create_banner(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/banner [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('img_url', 'description', 'order_no');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/banner [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #create banner
        $request['id']  = get_uniq_id();
        $flag           = $this->banner_model->create_banner($request);
        
        #response
        if(!$flag){
            logging('error', '/api/banner [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/banner [POST] - Create banner success', $request);
        $resp_obj->set_response(200, "success", "Create banner success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/banner [PUT]
    function update_banner(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/banner [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'img_url', 'description', 'order_no');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/banner [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check banner
        $banner = $this->banner_model->get_banner_by_id($request['id']);
        if(is_null($banner)){
            logging('error', '/api/banner [PUT] - banner not found', $request);
            $resp_obj->set_response(404, "failed", "banner not found");
            set_output($resp_obj->get_response());
            return;
        }

        $flag = $this->banner_model->update_banner($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/banner [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/banner [PUT] - Update banner success', $request);
        $resp_obj->set_response(200, "success", "Update banner success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/banner/$id [DELETE]
    function delete_banner($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/banner/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check banner
        $banner = $this->banner_model->get_banner_by_id($id);
        if(is_null($banner)){
            logging('error', '/api/banner/'.$id.' [DELETE] - banner not found');
            $resp_obj->set_response(404, "failed", "banner not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update banner
        $flag = $this->banner_model->delete_banner($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/banner/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/banner/'.$id.' [DELETE] - Delete banner success');
        $resp_obj->set_response(200, "success", "Delete banner success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/banner/upload [POST]
    function upload_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/banner/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/banner/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/banner/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/banner/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data                   = $resp['data'];
        $data['full_file_url']  = base_url($data['file_url']);
        
        logging('debug', '/api/banner/upload [POST] - Upload image success', $data);
        $resp_obj->set_response(200, "success", "Upload image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}
