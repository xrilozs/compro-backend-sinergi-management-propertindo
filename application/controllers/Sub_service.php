<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_service extends CI_Controller {

	public function sub_service_list(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/sub_service.js?v=').time()
			),
			"title"		=> "Sub Servis"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/sub_service/page');
		$this->load->view('layout/main_footer', $data);
	}
}
