<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/admin.js?v=').time()
			),
			"title"		=> "Admin"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/admin/page');
		$this->load->view('layout/main_footer', $data);
	}
}
