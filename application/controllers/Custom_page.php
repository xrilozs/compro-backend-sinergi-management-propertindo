<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Custom_page extends CI_Controller {
        public function page(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/custom_page.js?v=').time()
                ),
                "title"		=> "Halaman Kustom"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/custom_page/page');
            $this->load->view('layout/main_footer', $data);
        }

    }
?>