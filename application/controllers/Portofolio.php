<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portofolio extends CI_Controller {

	public function portofolio_list(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/portofolio.js?v=').time()
			),
			"title"		=> "Portofolio"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/portofolio/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function portofolio_image(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/portofolio_image.js?v=').time()
			),
			"title"		=> "Gambar Portofolio"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/portofolio/image');
		$this->load->view('layout/main_footer', $data);
	}

	public function portofolio_detail(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/portofolio_detail.js?v=').time()
			),
			"title"		=> "Detail Portofolio"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/portofolio/detail');
		$this->load->view('layout/main_footer', $data);
	}
}
