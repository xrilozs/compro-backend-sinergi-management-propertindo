<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Banner extends CI_Controller {
        public function page(){
            $data = array(
                "js_file"   => array(
                    base_url('assets/js/banner.js?v=').time()
                ),
                "title"		=> "Banner"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/banner/page');
            $this->load->view('layout/main_footer', $data);
        }

    }
?>