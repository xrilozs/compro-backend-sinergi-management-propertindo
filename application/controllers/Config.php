<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Config extends CI_Controller {
        public function page(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/config.js?v=').time()
                ),
                "title"		=> "Pengaturan"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/config/page');
            $this->load->view('layout/main_footer', $data);
        }

    }
?>