<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Faq extends CI_Controller {
        public function page(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/faq.js?v=').time()
                ),
                "title"		=> "FAQs"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/faq/page');
            $this->load->view('layout/main_footer', $data);
        }

    }
?>