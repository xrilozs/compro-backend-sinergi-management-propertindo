<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Message extends CI_Controller {
        public function page(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/message.js?v=').time()
                ),
                "title"		=> "Pesan"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/message/page');
            $this->load->view('layout/main_footer', $data);
        }

    }
?>