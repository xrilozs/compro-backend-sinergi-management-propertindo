<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {

	public function service_list(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/service.js?v=').time()
			),
			"title"		=> "Servis"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/service/list');
		$this->load->view('layout/main_footer', $data);
	}

	public function service_detail(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/service_deail.js?v=').time()
			),
			"title"		=> "Servis"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/service/detail');
		$this->load->view('layout/main_footer', $data);
	}
}
