<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" => array(
				base_url('assets/js/category.js?v=').time()
			),
			"title"		=> "Kategori"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/category/page');
		$this->load->view('layout/main_footer', $data);
	}
}
