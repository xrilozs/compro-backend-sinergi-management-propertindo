<?php
  require 'vendor/autoload.php';
  use Firebase\JWT\JWT;

  function verify_admin_token($header, $allowed_role=null){
    $CI       =& get_instance();
    $resp_obj = new Response_api();
    
    #check header
    if(isset($header['Authorization'])){
      list(, $token) = explode(' ', $header['Authorization']);
    }else{
      $resp_obj->set_response(401, "failed", "Please use token to access this resource.");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check token
    try {
      $jwt = JWT::decode($token, ACCESS_TOKEN_SALT, ['HS256']);
    } catch (Exception $e) {
      $resp_obj->set_response(401, "failed", "Invalid requested token");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    $username = $jwt->username;
    $admin    = $CI->admin_model->get_admin_by_username($username);
    
    #check Admin exist
    if(is_null($admin)){
      $resp_obj->set_response(401, "failed", "Admin not found");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    if(!$admin->is_active){
      $resp_obj->set_response(401, "failed", "Admin is Inactive");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check allowed role
    // if($allowed_role && !in_array($admin->role_id, $allowed_role)){
    //   $resp_obj->set_response(401, "failed", "Unauthorized role access", $allowed_role);
    //   $resp = $resp_obj->get_response();
    //   return $resp;
    // }
    
    $data = array('admin'=>$admin);
    $resp_obj->set_response(200, "success", "Authorized access", $data);
    $resp = $resp_obj->get_response();
    return $resp;
  }
?>