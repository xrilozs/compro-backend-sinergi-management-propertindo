<?php
    function send_order_email($email, $message, $product=null, $service=null){
        $CI                 =& get_instance();
        $message['brand']   = COMPANY_NAME;

        if($product || $service){
            $message['product_or_service']      = $product ? 'Produk' : 'Servis';
            $message['product_or_service_name'] = $product ? $product->name : $service->name;
            if($product){
                $message['product_or_service_price'] = $product->price;
            }
        }

        $html       = $CI->load->view('template/order_email', $message, true);
        $mail       = new Send_mail();
        $emailResp  = $mail->send($email, 'Ada Pesanan Baru', $html);
        return $emailResp;
    }
?>