<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style>
    	body{
    	    font-family: Helvetica, sans-serif;
    	    height: 100%;
            margin: 0px;
        }
        img.center {
            display: block;
            margin: 0 auto;
        }
        #container {
            width:100%; 
            height:100vh;
    	    background-color: crimson;
        }
        table, th, td {
            border: 10px solid crimson;
            border-collapse: collapse;
        }
        .blank-side {
            width:15%
        }
        .body {
            width:70%;
            background-color:white;
            /* margin: 20px;*/
            padding:10px;
            /* border-radius: 10px; */
        }
        #detail{
            text-align: center;
        }
        #header {
            text-align: center;
            /* margin-top: 20px;
            margin-bottom: 20px; */
        }
        #logo {
            width: 100px;
        }
        #google-play{
            width: 100px;
        }
        #content {
            text-align: center;
            font-size: 20px;
        }
        #footer {
            /* padding-top: 20px; */
            text-align: center;
            font-family: "Open Sans", sans-serif;
            font-size: 12px;
        }
        #footer-title {
            font-weight: bold;
            color: #d79922;
            font-size: 14px;
        }
    </style>
    </head>
    <body>
    <table id="container">
      <tr>
        <td class="blank-side"></td>
        <td>
            <table width="100%">
      		    <tr><td></td></tr>
                <tr>
                    <td class="body">
                        <div>
                            <div id="header">
                                <h1>Pesan Masuk</h1>
                            </div>
			            </div>
                    </td>
                </tr>
      		    <tr>
      		        <td class="body">
      		    	    <div>
                            <div id="content">
                                <p>Ada pesan baru dari website. Berikut detailnya:</p>
                                <p>
                                    <b>Pengirim: </b><?=$fullname;?><br>
                                    <b>Nomor Telepon: </b><?=$phone;?><br>
                                    <b>Email: </b><?=$email;?><br>
                                    <b>Perusahaan: </b><?=$company;?><br>
                                    <b>Pesan: </b><?=$content;?><br>
                                </p>
                            </div>
			            </div>
      		        </td>
      		    </tr>
                <?php if($has_source): ?>
                <tr>
                    <td class="body">
                        <div>
                          <div id="content">
                              <p>Detail <?=$product_or_service;?>:</p>
                              <p>
                                    <b>Nama: </b><?=$product_or_service_name;?><br>
                                    <?php if(isset($product_or_service_price)): ?>
                                        <b>Harga: </b>Rp <?=number_format($product_or_service_price, 2, ',', '.');?><br>
                                    <?php endif; ?>
                              </p>
                          </div>
                      </div>
                    </td>
                </tr>
                <?php endif; ?>
                <tr>
                    <td class="body">
                        <div>
                            <div id="footer">
                                &copy; Copyright <span id="footer-title"><?=$brand;?></span>. All Rights Reserved
                            </div>
			            </div>
                    </td>
                </tr>
      		    <tr><td></td></tr>
		    </table>
        </td>
        <td class="blank-side"></td>
      </tr>
    </table>
    </body>
</html>