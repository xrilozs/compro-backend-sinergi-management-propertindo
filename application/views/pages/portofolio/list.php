<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Portofolio</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Portofolio</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="portofolio-create-toggle" data-toggle="modal" data-target="#portofolio-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="portofolio-datatable">
                  <thead>
                    <tr>
                      <th>Judul</th>
                      <th>Kategori</th>
                      <th>Konten</th>
                      <th>Tanggal</th>
                      <th>Klien</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="portofolio-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Portofolio</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="portofolio-create-form">
        <div class="form-group">
          <label for="portofolio-date-create-field">Tanggal:</label>
          <input type="text" name="date" class="form-control datepicker" id="portofolio-date-create-field" placeholder="Tanggal.." required>
        </div>
        <div class="form-group">
          <label for="portofolio-title-create-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="portofolio-title-create-field" placeholder="Judul.." required>
        </div>
        <div class="form-group">
          <label for="portofolio-category-create-field">Kategori:</label>
          <select name="category" id="portofolio-category-create-field" class="form-control" required>
            <option value="SERVICE">Servis</option>
            <option value="PRODUCT">Produk</option>
          </select>
        </div>
        <div class="form-group">
          <label for="portofolio-content-create-field">Konten:</label>
          <textarea name="content" class="form-control rich-text-create" id="portofolio-content-create-field" placeholder="Konten.." required></textarea>
        </div>
        <div class="form-group">
          <label for="portofolio-client-create-field">Klien:</label>
          <input type="text" name="client" class="form-control" id="portofolio-client-create-field" placeholder="Klien.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="portofolio-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="portofolio-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Portofolio</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="portofolio-update-form">
        <div class="form-group">
          <label for="portofolio-date-update-field">Tanggal:</label>
          <input type="text" name="date" class="form-control datepicker" id="portofolio-date-update-field" placeholder="Tanggal.." required>
        </div>
        <div class="form-group">
          <label for="portofolio-title-update-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="portofolio-title-update-field" placeholder="Judul.." required>
        </div>
        <div class="form-group">
          <label for="portofolio-category-update-field">Kategori:</label>
          <select name="category" id="portofolio-category-update-field" class="form-control" required>
            <option value="SERVICE">Servis</option>
            <option value="PRODUCT">Produk</option>
          </select>
        </div>
        <div class="form-group">
          <label for="portofolio-content-update-field">Konten:</label>
          <textarea name="content" class="form-control rich-text-update" id="portofolio-content-update-field" placeholder="Konten.." required></textarea>
        </div>
        <div class="form-group">
          <label for="portofolio-client-update-field">Klien:</label>
          <input type="text" name="client" class="form-control" id="portofolio-client-update-field" placeholder="Klien.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="portofolio-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="portofolio-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Portofolio</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus portofolio ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="portofolio-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="portofolio-active-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aktifasi Portofolio</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengaktifkan portofolio ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="portofolio-active-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="portofolio-inactive-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nonaktifkan Portofolio</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menonaktifkan portofolio ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="portofolio-inactive-button">Ya</button>
      </div>
    </div>
  </div>
</div>