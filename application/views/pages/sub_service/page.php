<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Sub-servis</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item">Servis</li>
            <li class="breadcrumb-item active">Sub-servis</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row mb-3">
                <div class="col-6">
                  <label>Pilih Servis:</label>
                  <select name="service-filter" id="service-filter" class="form-control service-option">-</select>
                </div>
                <div class="col-6 text-right">
                  <button type="button" class="btn btn-primary" id="subservice-create-toggle" data-toggle="modal" data-target="#subservice-create-modal">
                    <i class="fas fa-plus"></i> Buat
                  </button>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="subservice-datatable">
                  <thead>
                    <tr>
                      <th>Gambar</th>
                      <th>Servis</th>
                      <th>Sub servis</th>
                      <th>Deskripsi</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="subservice-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Sub-Servis</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="subservice-create-form">
        <div class="form-group">
          <label>Gambar</label>
          <input 
            type="file"
            id="subservice-create-image"
            data-allowed-file-extensions="jpeg jpg png"
          >
        </div>
        <div class="form-group">
          <label for="subservice-service-create-field">Servis:</label>
          <select name="service" class="form-control service-option" id="subservice-service-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="subservice-name-create-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="subservice-name-create-field" placeholder="Nama.." required>
        </div>
        <div class="form-group">
          <label for="subservice-description-create-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="subservice-description-create-field" placeholder="Deskripsi.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="subservice-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="subservice-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Sub-Servis</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="subservice-update-form">
        <div class="form-group">
          <label>Gambar</label>
          <input 
            type="file"
            id="subservice-update-image"
            data-allowed-file-extensions="jpeg jpg png"
          >
        </div>
        <div class="form-group">
          <label for="subservice-service-update-field">Servis:</label>
          <select name="service" class="form-control service-option" id="subservice-service-update-field" required></select>
        </div>
        <div class="form-group">
          <label for="subservice-name-update-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="subservice-name-update-field" placeholder="Nama.." required>
        </div>
        <div class="form-group">
          <label for="subservice-description-update-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="subservice-description-update-field" placeholder="Deskripsi.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="subservice-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="subservice-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Servis</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus servis ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="subservice-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="subservice-active-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aktifasi Servis</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengaktifkan servis ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="subservice-active-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="subservice-inactive-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nonaktifkan Servis</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menonaktifkan servis ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="subservice-inactive-button">Ya</button>
      </div>
    </div>
  </div>
</div>
