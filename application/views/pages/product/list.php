<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Produk</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Produk</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="product-create-toggle" data-toggle="modal" data-target="#product-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="product-datatable">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Deskripsi</th>
                      <!-- <th>Harga</th> -->
                      <th>Kategori</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="product-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Produk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="product-create-form">
        <div class="form-group">
          <label for="product-name-create-field">Nama Produk:</label>
          <input type="text" name="name" class="form-control" id="product-name-create-field" placeholder="Nama produk.." required>
        </div>
        <div class="form-group">
          <label for="product-description-create-field">Deskripsi:</label>
          <textarea name="description" class="form-control rich-text-create" id="product-description-create-field" placeholder="Deskripsi.." required></textarea>
        </div>
        <!-- <div class="form-group">
          <label for="product-price-create-field">Harga:</label>
          <input type="number" name="price" class="form-control" id="product-price-create-field" placeholder="Harga.." required>
        </div> -->
        <div class="form-group">
          <label for="product-category-create-field">Kategori:</label>
          <select name="category" class="form-control category-option" id="product-category-create-field" required></select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="product-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="product-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Produk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="product-update-form">
        <div class="form-group">
          <label for="product-name-update-field">Nama Produk:</label>
          <input type="text" name="name" class="form-control" id="product-name-update-field" placeholder="Nama produk.." required>
        </div>
        <div class="form-group">
          <label for="product-description-update-field">Deskripsi:</label>
          <textarea name="description" class="form-control rich-text-update" id="product-description-update-field" placeholder="Deskripsi.." required></textarea>
        </div>
        <!-- <div class="form-group">
          <label for="product-price-update-field">Harga:</label>
          <input type="number" name="price" class="form-control" id="product-price-update-field" placeholder="Harga.." required>
        </div> -->
        <div class="form-group">
          <label for="product-category-update-field">Kategori:</label>
          <select name="category" class="form-control category-option" id="product-category-update-field" required></select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="product-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="product-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Produk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus produk ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="product-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="product-active-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aktifasi Produk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengaktifkan produk ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="product-active-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="product-inactive-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nonaktifkan Produk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menonaktifkan produk ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="product-inactive-button">Ya</button>
      </div>
    </div>
  </div>
</div>