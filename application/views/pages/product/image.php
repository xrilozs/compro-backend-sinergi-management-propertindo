<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Gambar Produk</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=base_url('product');?>">Produk</a></li>
            <li class="breadcrumb-item active">Gambar</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-6">
            <a href="<?=base_url('product');?>" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Kembali
            </a>
        </div>
        <div class="col-6 text-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#image-add-modal">
                <i class="fas fa-plus"></i> Tambah
            </button>
        </div>
      </div>
      <div class="row mt-5" id="product-image-section">
        <div class="col-12 d-flex justify-content-center" id="product-image-loading">
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="image-add-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Gambar Produk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="image-add-form">
        <div class="form-group">
            <label>Gambar</label>
            <input 
                type="file"
                id="image-add-input"
                data-allowed-file-extensions="jpeg jpg png"
                required
            >
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="image-add-button">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="image-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Gambar Produk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus gambar produk ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="image-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>
