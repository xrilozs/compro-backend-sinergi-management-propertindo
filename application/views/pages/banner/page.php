<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Banner</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Banner</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="banner-create-toggle" data-toggle="modal" data-target="#banner-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="banner-datatable">
                  <thead>
                    <tr>
                      <th>Gambar</th>
                      <th>Urutan</th>
                      <th>Deskripsi</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="banner-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Banner</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="banner-create-form">
        <div class="form-group">
          <label>Gambar</label>
          <input 
            type="file" 
            class="dropify"
            id="banner-create-image"
            data-allowed-file-extensions="jpeg jpg png"
          >
        </div>
        <div class="form-group">
          <label for="banner-orderno-create-field">Urutan:</label>
          <input type="number" name="order_no" class="form-control" id="banner-orderno-create-field" placeholder="Urutan.." required>
        </div>
        <div class="form-group">
          <label for="banner-description-create-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="banner-description-create-field" placeholder="Deskripsi.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="banner-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="banner-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Banner</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="banner-update-form">
        <div class="form-group">
          <label>Gambar</label>
          <input 
            type="file" 
            id="banner-update-image"
            data-allowed-file-extensions="jpeg jpg png"
          >
        </div>
        <div class="form-group">
          <label for="banner-orderno-update-field">Urutan:</label>
          <input type="number" name="order_no" class="form-control" id="banner-orderno-update-field" placeholder="Urutan.." required>
        </div>
        <div class="form-group">
          <label for="banner-description-update-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="banner-description-update-field" placeholder="Deskripsi.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="banner-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="banner-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Banner</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus banner tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="banner-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

