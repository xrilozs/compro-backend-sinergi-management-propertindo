<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pengaturan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Pengaturan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-address-book"></i> Kontak</h3>
            </div>
            <div class="card-body">
              <form id="contact-form">
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label>Email</label><br>
                      <input id="email" name="email" placeholder="Support Email.." required>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Phone</label><br>
                      <input id="phone" name="phone" placeholder="Phone.." required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Address</label>
                      <textarea class="form-control" id="address" name="address" placeholder="Address.." required></textarea>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-globe-asia"></i> Sosial media</h3>
            </div>
            <div class="card-body">
              <form id="social-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Facebook</label>
                      <input type="text" class="form-control" id="facebook" name="sm_facebook" placeholder="Facebook..">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Twitter</label>
                      <input type="text" class="form-control" id="twitter" name="sm_twitter" placeholder="Twitter..">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Instagram</label>
                      <input type="text" class="form-control" id="instagram" name="sm_instagram" placeholder="Instagram..">
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-globe-asia"></i> Website</h3>
            </div>
            <div class="card-body">
              <form id="application-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Deskripsi Web</label>
                      <textarea class="form-control" id="web-description" name="web-description" placeholder="Web Description.." ></textarea>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="config-logo-field">Logo (*.jpg, *.png):</label>
                      <input 
                        type="file" 
                        id="config-upload-logo"
                        data-allowed-file-extensions="jpeg jpg png" 
                      >
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="config-icon-field">Icon (*.ico):</label>
                      <input 
                        type="file" 
                        id="config-upload-icon"
                        data-allowed-file-extensions="ico" 
                      >
                    </div>
                  </div>
                </div>
              </form>
                <div class="row">
                  <div class="col-12">
                    <button type="button" class="btn btn-primary btn-block" id="config-button">Simpan</button>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="config-uploadLogo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Logo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="config-uploadLogo-form">          
        <div class="form-group">
          <label for="config-uploadLogo-field">Logo:</label>
          <input type="file" class="form-control-file" id="config-uploadLogo-field" name="image" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="config-uploadLogo-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="config-uploadIcon-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Icon</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="config-uploadIcon-form">          
        <div class="form-group">
          <label for="config-uploadIcon-field">Icon:</label>
          <input type="file" class="form-control-file" id="config-uploadIcon-field" name="image" accept="image/ico" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="config-uploadIcon-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
