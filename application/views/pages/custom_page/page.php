<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Halaman Kustom</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Halaman Kustom</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="page-create-toggle" data-toggle="modal" data-target="#page-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="page-datatable">
                  <thead>
                    <tr>
                      <th>Judul</th>
                      <th>Alias</th>
                      <th>Posisi</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="page-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Halaman Kustom</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="page-create-form">
        <div class="form-group">
          <label for="page-title-create-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="page-title-create-field" placeholder="page title.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="page-position-create-field">Posisi:</label>
          <select name="position" class="form-control" id="page-position-create-field">
            <option value="FOOTER">Footer</option>
            <option value="HEADER">Header</option>
            <option value="BOTH">Semua</option>
          </select>
        </div>
        <div class="form-group">
          <label for="page-content-create-field">Konten:</label>
          <textarea name="content" class="form-control rich-text-create" id="page-content-create-field" required>
          </textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="page-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="page-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="page-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Halaman Kustom</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="page-update-form">
        <div class="form-group">
          <label for="page-name-update-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="page-title-update-field" placeholder="page title.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="page-alias-update-field">Alias:</label>
          <input type="text" name="alias" class="form-control" id="page-alias-update-field" placeholder="page alias.." disabled>
        </div>
        <div class="form-group">
          <label for="page-position-update-field">Posisi:</label>
          <select name="position" class="form-control" id="page-position-update-field">
            <option value="FOOTER">Footer</option>
            <option value="HEADER">Header</option>
            <option value="BOTH">Semua</option>
          </select>
        </div>
        <div class="form-group">
          <label for="page-content-update-field">Konten:</label>
          <textarea name="content" class="form-control rich-text-update" id="page-content-update-field" required>
          </textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="page-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="page-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Halaman Kustom</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus halaman kustom tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="page-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="page-inactive-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nonaktif Halaman Kustom</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menonaktifkan halaman kustom ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="page-inactive-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="page-active-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aktifasi Halaman Kustom</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        apakah Anda yakin ingin aktifkan halaman kustom?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-info" id="page-active-button">Ya</button>
      </div>
    </div>
  </div>
</div>