<!-- Content Wrapper. Contains message content -->
<div class="content-wrapper">
  <!-- Content Header (message header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pesan Masuk</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Pesan Masuk</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="message-datatable">
                  <thead>
                    <tr>
                      <th>Nama Pengirim</th>
                      <th>No. HP</th>
                      <th>Email</th>
                      <th>Perusahaan</th>
                      <th>Produk</th>
                      <th>Servis</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="message-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Pesan Masuk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="message-detail-form">
        <div class="form-group">
          <label for="message-fullname-detail-field">Nama Pengirim:</label>
          <input type="text" name="fullname" class="form-control" id="message-fullname-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="message-phone-detail-field">No. HP:</label>
          <input type="text" name="phone" class="form-control" id="message-phone-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="message-email-detail-field">Email:</label>
          <input type="text" name="email" class="form-control" id="message-email-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="message-company-detail-field">Perusahaan:</label>
          <input type="text" name="company" class="form-control" id="message-company-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="message-content-detail-field">Pesan:</label>
          <textarea name="content" class="form-control" id="message-content-detail-field" readonly>
          </textarea>
        </div>
        <div class="form-group" id="message-product-section">
          <label>Produk:</label>
          <div class="row">
            <div class="col-12">
              <p id="message-product-detail-title"></p>
            </div>
            <div class="col-12" id="message-product-detail-image"></div>
          </div>
        </div>
        <div class="form-group" id="message-service-section">
          <label>Servis:</label>
          <div class="row">
            <div class="col-12">
              <p id="message-product-detail-title"></p>
            </div>
            <div class="col-12" id="message-service-detail-description"></div>
          </div>
        </div>
        <div class="form-group">
          <label for="message-createdat-detail-field">Tanggal dikirim:</label>
          <input type="text" name="createdat" class="form-control" id="message-createdat-detail-field" readonly>
        </div>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>
