<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3 id="dashboard-product-total">0</h3>
              <p>Produk</p>
            </div>
            <div class="icon">
            <i class="fas fa-solar-panel"></i>
            </div>
            <a href="<?=base_url('product');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3 id="dashboard-service-total">0</h3>
              <p>Servis</p>
            </div>
            <div class="icon">
            <i class="fas fa-tools"></i>
            </div>
            <a href="<?=base_url('service');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3 id="dashboard-portofolio-total">0</h3>
              <p>Portofolio</p>
            </div>
            <div class="icon">
            <i class="fas fa-building"></i>
            </div>
            <a href="<?=base_url('portofolio');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3 id="dashboard-category-total">0</h3>
              <p>Kategori</p>
            </div>
            <div class="icon">
            <i class="fas fa-tags"></i>
            </div>
            <a href="<?=base_url('category');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3 id="dashboard-message-total">0</h3>
              <p>Pesan</p>
            </div>
            <div class="icon">
            <i class="fas fa-envelope"></i>
            </div>
            <a href="<?=base_url('message');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>