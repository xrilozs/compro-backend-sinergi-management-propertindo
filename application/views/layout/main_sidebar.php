  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-1">
    <!-- Brand Logo -->
    <a href="<?=base_url('dashboard');?>" class="brand-link">
      <img src="<?=base_url('assets/img/default-logo.png');?>" alt="Logo" class="brand-image" style="opacity: .8">
      <span class="font-weight-bold">Sinergi MP</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item" id="sidebar-dashboard-menu">
            <a href="<?=base_url('dashboard');?>" class="nav-link <?=$title == 'Dashboard' ? 'active' : '';?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item <?=$title == 'Kategori' || $title == 'Produk' ? 'menu-is-opening menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-solar-panel"></i>
              <p>
                Produk
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item" id="sidebar-category-menu">
                <a href="<?=base_url('category');?>" class="nav-link <?=$title == 'Kategori' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-tags"></i>
                  <p>
                    Kategori
                  </p>
                </a>
              </li>
              <li class="nav-item" id="sidebar-website-menu">
                <a href="<?=base_url('product');?>" class="nav-link <?=$title == 'Produk' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-solar-panel"></i>
                  <p>Produk</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?=$title == 'Servis' || $title == 'Sub Servis' ? 'menu-is-opening menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tools"></i>
              <p>
                Servis
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item" id="sidebar-service-menu">
                <a href="<?=base_url('service');?>" class="nav-link <?=$title == 'Servis' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-tools"></i>
                  <p>
                    Servis
                  </p>
                </a>
              </li>
              <li class="nav-item" id="sidebar-website-menu">
                <a href="<?=base_url('sub-service');?>" class="nav-link <?=$title == 'Sub Servis' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-tools"></i>
                  <p>Sub Servis</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item" id="sidebar-portofolio-menu">
            <a href="<?=base_url('portofolio');?>" class="nav-link <?=$title == 'Portofolio' ? 'active' : '';?>">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Portofolio
              </p>
            </a>
          </li>
          <li class="nav-item" id="sidebar-message-menu">
            <a href="<?=base_url('message');?>" class="nav-link <?=$title == 'Pesan' ? 'active' : '';?>">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Pesan Masuk
              </p>
            </a>
          </li>
          <li class="nav-item " id="sidebar-banner-menu">
            <a href="<?=base_url('banner');?>" class="nav-link <?=$title == 'Banner' ? 'active' : '';?>">
              <i class="nav-icon fas fa-image"></i>
              <p>
                Banner
              </p>
            </a>
          </li>
          <li class="nav-item " id="sidebar-page-menu">
            <a href="<?=base_url('custom-page');?>" class="nav-link <?=$title == 'Halaman Kustom' ? 'active' : '';?>">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Halaman Kustom
              </p>
            </a>
          </li>
          <li class="nav-item" id="sidebar-faq-menu">
            <a href="<?=base_url('faq');?>" class="nav-link <?=$title == 'FAQs' ? 'active' : '';?>">
              <i class="nav-icon fas fa-question-circle"></i>
              <p>
                FAQs
              </p>
            </a>
          </li>
          <li class="nav-item" id="sidebar-admin-menu">
            <a href="<?=base_url('admin');?>" class="nav-link <?=$title == 'Admin' ? 'active' : '';?>">
              <i class="nav-icon fas fa-users-cog"></i>
              <p>
                Admin
              </p>
            </a>
          </li>
          <li class="nav-item" id="sidebar-config-menu">
            <a href="<?=base_url('config');?>" class="nav-link <?=$title == 'Pengaturan' ? 'active' : '';?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
