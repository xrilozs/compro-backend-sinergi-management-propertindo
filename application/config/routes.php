<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* CMS Route */
#Home
$route['login']         = 'common/login';
$route['dashboard']     = 'common/dashboard';
#Admin
$route['admin']         = 'admin/page';
#Admin
$route['banner']         = 'banner/page';
#Category
$route['category']      = 'category/page';
#Product
$route['product']               = 'product/product_list';
$route['product/image/(:any)']  = 'product/product_image';
$route['product/detail/(:any)'] = 'product/product_detail';
#Portofolio
$route['portofolio']                = 'portofolio/portofolio_list';
$route['portofolio/image/(:any)']   = 'portofolio/portofolio_image';
$route['portofolio/detail/(:any)']  = 'portofolio/portofolio_detail';
#Service
$route['service']               = 'service/service_list';
$route['service/detail/(:any)'] = 'service/service_detail';
#Sub Service
$route['sub-service']   = 'sub_service/sub_service_list';
#Config
$route['config']        = 'config/page';
#FAQ
$route['faq']           = 'faq/page';
#Page
$route['custom-page']   = 'custom_page/page';
#Message
$route['message']       = 'message/page';
#Banner
$route['banner']        = 'banner/page';

/* API Route */
#Common
$route['api/test']          = 'api/common/test';
$route['api/test-email']    = 'api/common/test_email';
#Admin
$route['api/admin']['GET']                  = 'api/admin/get_admin';
$route['api/admin/id/(:any)']['GET']        = 'api/admin/get_admin_by_id/$1';
$route['api/admin']['POST']                 = 'api/admin/create_admin';
$route['api/admin']['PUT']                  = 'api/admin/update_admin';
$route['api/admin/active/(:any)']['PUT']    = 'api/admin/active_admin/$1';
$route['api/admin/inactive/(:any)']['PUT']  = 'api/admin/inactive_admin/$1';
$route['api/admin/password']['PUT']         = 'api/admin/change_password_admin';
$route['api/admin/login']['POST']           = 'api/admin/login';
$route['api/admin/refresh']['POST']         = 'api/admin/login_admin';
$route['api/admin/profile']['GET']          = 'api/admin/get_admin_profile';
#Banner
$route['api/banner']['GET']                    = 'api/banner/get_banner';
$route['api/banner']['POST']                   = 'api/banner/create_banner';
$route['api/banner']['PUT']                    = 'api/banner/update_banner';
$route['api/banner/delete/(:any)']['DELETE']   = 'api/banner/delete_banner/$1';
$route['api/banner/id/(:any)']['GET']          = 'api/banner/get_banner_by_id/$1';
$route['api/banner/all']['GET']                = 'api/banner/get_banner_all';
$route['api/banner/upload']['POST']            = 'api/banner/upload_image';
#Config
$route['api/config']['GET']         = 'api/config/get_config';
$route['api/config']['POST']        = 'api/config/save_config';
$route['api/config/upload']['POST'] = 'api/config/upload_config_image';
#Custom Page
$route['api/custom-page']['GET']                    = 'api/custom_page/get_custom_page';
$route['api/custom-page']['POST']                   = 'api/custom_page/create_custom_page';
$route['api/custom-page']['PUT']                    = 'api/custom_page/update_custom_page';
$route['api/custom-page/delete/(:any)']['DELETE']   = 'api/custom_page/delete_custom_page/$1';
$route['api/custom-page/id/(:any)']['GET']          = 'api/custom_page/get_custom_page_by_id/$1';
$route['api/custom-page/alias/(:any)']['GET']       = 'api/custom_page/get_custom_page_by_alias/$1';
$route['api/custom-page/active/(:any)']['PUT']      = 'api/custom_page/active_custom_page/$1';
$route['api/custom-page/inactive/(:any)']['PUT']    = 'api/custom_page/inactive_custom_page/$1';
$route['api/custom-page/position']['GET']           = 'api/custom_page/get_custom_page_by_position';
#FAQ
$route['api/faq']['GET']                    = 'api/faq/get_faq';
$route['api/faq']['POST']                   = 'api/faq/create_faq';
$route['api/faq']['PUT']                    = 'api/faq/update_faq';
$route['api/faq/delete/(:any)']['DELETE']   = 'api/faq/delete_faq/$1';
$route['api/faq/id/(:any)']['GET']          = 'api/faq/get_faq_by_id/$1';
$route['api/faq/all']['GET']                = 'api/faq/get_faq_all';
#Message
$route['api/message']['GET']                = 'api/message/get_message';
$route['api/message']['POST']               = 'api/message/create_message';
$route['api/message/id/(:any)']['GET']      = 'api/message/get_message_by_id/$1';
$route['api/message/read/(:any)']['POST']   = 'api/message/read_message/$1';
$route['api/message/read-all']['POST']      = 'api/message/read_message_all';
$route['api/message/total']['GET']          = 'api/message/get_message_total';
#Portofolio
$route['api/portofolio/all']['GET']                 = 'api/portofolio/get_portofolio_all';
$route['api/portofolio']['GET']                     = 'api/portofolio/get_portofolio';
$route['api/portofolio']['POST']                    = 'api/portofolio/create_portofolio';
$route['api/portofolio']['PUT']                     = 'api/portofolio/update_portofolio';
$route['api/portofolio/delete/(:any)']['DELETE']    = 'api/portofolio/delete_portofolio/$1';
$route['api/portofolio/active/(:any)']['PUT']       = 'api/portofolio/active_portofolio/$1';
$route['api/portofolio/inactive/(:any)']['PUT']     = 'api/portofolio/inactive_portofolio/$1';
$route['api/portofolio/id/(:any)']['GET']           = 'api/portofolio/get_portofolio_by_id/$1';
$route['api/portofolio/alias/(:any)']['GET']        = 'api/portofolio/get_portofolio_by_alias/$1';
$route['api/portofolio/total']['GET']               = 'api/portofolio/get_portofolio_total';
#Portofolio Image
$route['api/portofolio/image/upload']['POST']           = 'api/portofolio_image/upload_portofolio_image';
$route['api/portofolio/image']['POST']                  = 'api/portofolio_image/add_portofolio_image';
$route['api/portofolio/image/delete/(:any)']['DELETE']  = 'api/portofolio_image/remove_portofolio_image/$1';
$route['api/portofolio/image/(:any)']['GET']            = 'api/portofolio_image/get_portofolio_image_by_portofolio/$1';
#Product Category
$route['api/product-category']['GET']                   = 'api/product_category/get_product_category';
$route['api/product-category']['POST']                  = 'api/product_category/create_product_category';
$route['api/product-category']['PUT']                   = 'api/product_category/update_product_category';
$route['api/product-category/active/(:any)']['PUT']     = 'api/product_category/active_product_category/$1';
$route['api/product-category/inactive/(:any)']['PUT']   = 'api/product_category/inactive_product_category/$1';
$route['api/product-category/delete/(:any)']['DELETE']  = 'api/product_category/delete_product_category/$1';
$route['api/product-category/id/(:any)']['GET']         = 'api/product_category/get_product_category_by_id/$1';
$route['api/product-category/alias/(:any)']['GET']      = 'api/product_category/get_product_category_by_alias/$1';
$route['api/product-category/all']['GET']               = 'api/product_category/get_product_category_all';
$route['api/product-category/total']['GET']             = 'api/product_category/get_product_category_total';
$route['api/product-category/upload']['POST']           = 'api/product_category/upload_image';
#Product
$route['api/product']['GET']                     = 'api/product/get_product';
$route['api/product']['POST']                    = 'api/product/create_product';
$route['api/product']['PUT']                     = 'api/product/update_product';
$route['api/product/active/(:any)']['PUT']       = 'api/product/active_product/$1';
$route['api/product/inactive/(:any)']['PUT']     = 'api/product/inactive_product/$1';
$route['api/product/delete/(:any)']['DELETE']    = 'api/product/delete_product/$1';
$route['api/product/category/(:any)']['GET']     = 'api/product/get_product_by_category/$1';
$route['api/product/id/(:any)']['GET']           = 'api/product/get_product_by_id/$1';
$route['api/product/alias/(:any)']['GET']        = 'api/product/get_product_by_alias/$1';
$route['api/product/total']['GET']               = 'api/product/get_product_total';
#Product Image
$route['api/product/image/upload']['POST']           = 'api/product_image/upload_product_image';
$route['api/product/image']['POST']                  = 'api/product_image/add_product_image';
$route['api/product/image/delete/(:any)']['DELETE']  = 'api/product_image/remove_product_image/$1';
$route['api/product/image/(:any)']['GET']            = 'api/product_image/get_product_image_by_product/$1';
#Service
$route['api/service']['GET']                    = 'api/service/get_service';
$route['api/service']['POST']                   = 'api/service/create_service';
$route['api/service']['PUT']                    = 'api/service/update_service';
$route['api/service/delete/(:any)']['DELETE']   = 'api/service/delete_service/$1';
$route['api/service/id/(:any)']['GET']          = 'api/service/get_service_by_id/$1';
$route['api/service/alias/(:any)']['GET']       = 'api/service/get_service_by_alias/$1';
$route['api/service/active/(:any)']['PUT']      = 'api/service/active_service/$1';
$route['api/service/inactive/(:any)']['PUT']    = 'api/service/inactive_service/$1';
$route['api/service/all']['GET']                = 'api/service/get_service_all';
$route['api/service/total']['GET']              = 'api/service/get_service_total';
#Sub Service
$route['api/sub-service']['POST']                   = 'api/sub_service/create_sub_service';
$route['api/sub-service']['PUT']                    = 'api/sub_service/update_sub_service';
$route['api/sub-service/delete/(:any)']['DELETE']   = 'api/sub_service/delete_sub_service/$1';
$route['api/sub-service/service/(:any)']['GET']     = 'api/sub_service/get_sub_service_by_service/$1';
$route['api/sub-service/id/(:any)']['GET']          = 'api/sub_service/get_sub_service_by_id/$1';
$route['api/sub-service/active/(:any)']['PUT']      = 'api/sub_service/active_sub_service/$1';
$route['api/sub-service/inactive/(:any)']['PUT']    = 'api/sub_service/inactive_sub_service/$1';
$route['api/sub-service/upload']['POST']            = 'api/sub_service/upload_image';
