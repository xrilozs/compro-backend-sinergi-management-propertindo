<?php
  class Portofolio_model extends CI_Model{
    public $id;
    public $title;
    public $category;
    public $alias;
    public $content;
    public $date;
    public $client;
    public $is_active;

    function get_portofolio($search=null, $category=null, $is_active=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', title, category, alias, content, date, client) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($category){
        $this->db->where("category", $category);
      }
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('portofolio');
      return $query->result();
    }

    function count_portofolio($search=null, $is_active=null){
      if($search){
        $where_search = "CONCAT_WS(',', title, category, alias, content, date, client) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $this->db->from('portofolio');
      return $this->db->count_all_results();
    }

    function get_portofolio_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('portofolio');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_portofolio_by_alias($alias, $is_assoc=false){
      $this->db->where("alias", $alias);
      $query = $this->db->get('portofolio');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_portofolio($data){
      $this->id         = $data['id'];
      $this->title      = $data['title'];
      $this->category   = $data['category'];
      $this->alias      = $data['alias'];
      $this->content    = $data['content'];
      $this->client     = $data['client'];
      $this->date       = $data['date'];
      $this->is_active  = 1;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('portofolio', $this);
      return $this->db->affected_rows();
    }

    function update_portofolio($data, $id){
      $this->db->update('portofolio', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_portofolio($id){
      $this->db->where('id', $id);
      $this->db->delete('portofolio');
      return $this->db->affected_rows();
    }

    function inactive_portofolio($id){
      $this->db->where('id', $id);
      $this->db->update('portofolio', array('is_active'=>0));
      return $this->db->affected_rows();
    }

    function active_portofolio($id){
      $this->db->where('id', $id);
      $this->db->update('portofolio', array('is_active'=>1));
      return $this->db->affected_rows();
    }
  }
?>
