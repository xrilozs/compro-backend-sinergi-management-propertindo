<?php
  class Service_model extends CI_Model{
    public $id;
    public $name;
    public $alias;
    public $description;
    public $is_active;

    function get_service($search=null, $is_active=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', name, alias, description) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('service');
      return $query->result();
    }

    function count_service($search=null, $is_active=null){
      if($search){
        $where_search = "CONCAT_WS(',', name, alias, description) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $this->db->from('service');
      return $this->db->count_all_results();
    }

    function get_service_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('service');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_service_by_alias($alias, $is_assoc=false){
      $this->db->where("alias", $alias);
      $this->db->where("is_active", 1);
      $query = $this->db->get('service');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_service($data){
      $this->id           = $data['id'];
      $this->name         = $data['name'];
      $this->alias        = $data['alias'];
      $this->description  = $data['description'];
      $this->is_active    = 1;
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('service', $this);
      return $this->db->affected_rows();
    }

    function update_service($data, $id){
      $this->db->update('service', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function inactive_service($id){
      $data = array(
        "is_active" => 0
      );
      $this->db->where('id', $id);
      $this->db->update('service', $data);
      return $this->db->affected_rows();
    }

    function active_service($id){
      $data = array(
        "is_active" => 1
      );
      $this->db->where('id', $id);
      $this->db->update('service', $data);
      return $this->db->affected_rows();
    }

    function delete_service($id){
      $this->db->where('id', $id);
      $this->db->delete('service');
      return $this->db->affected_rows();
    }
  }
?>
