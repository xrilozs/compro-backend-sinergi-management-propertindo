<?php
  class Banner_model extends CI_Model{
    public $id;
    public $img_url;
    public $description;
    public $order_no;

    function get_banner($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', description, order_no) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('banner');
      return $query->result();
    }

    function count_banner($search=null){
      if($search){
        $where_search = "CONCAT_WS(',', description, order_no) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      $this->db->from('banner');
      return $this->db->count_all_results();
    }

    function get_banner_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('banner');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_banner($data){
      $this->id           = $data['id'];
      $this->img_url      = $data['img_url'];
      $this->description  = $data['description'];
      $this->order_no     = $data['order_no'];
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('banner', $this);
      return $this->db->affected_rows();
    }

    function update_banner($data, $id){
      $this->db->update('banner', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_banner($id){
      $this->db->where('id', $id);
      $this->db->delete('banner');
      return $this->db->affected_rows();
    }
  }
?>
