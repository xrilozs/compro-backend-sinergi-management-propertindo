<?php
  class Config_model extends CI_Model{
    public $id;
    public $sm_twitter;
    public $sm_instagram;
    public $sm_facebook;
    public $sm_whatsapp;
    public $web_description;
    public $web_logo;
    public $web_icon;
    public $address;
    public $phone;
    public $email;

    function get_config($is_assoc=false){
      $this->db->from('config');
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_config_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from('config');
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_config($data){
      $this->id               = $data['id'];
      $this->sm_twitter       = $data['sm_twitter'];
      $this->sm_instagram     = $data['sm_instagram'];
      $this->sm_facebook      = $data['sm_facebook'];
      $this->sm_whatsapp      = $data['sm_whatsapp'];
      $this->web_description  = $data['web_description'];
      $this->web_logo         = $data['web_logo'];
      $this->web_icon         = $data['web_icon'];
      $this->address          = $data['address'];
      $this->phone            = $data['phone'];
      $this->email            = $data['email'];
      $this->db->insert('config', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_config($data, $id){
      $this->db->update('config', $data, array("id"=>$id));
      return $this->db->affected_rows();
    } 
  }
?>
