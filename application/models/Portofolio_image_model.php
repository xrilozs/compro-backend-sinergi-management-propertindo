<?php
  class Portofolio_image_model extends CI_Model{
    public $id;
    public $portofolio_id;
    public $img_url;

    function get_portofolio_image_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('portofolio_image');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_portofolio_image_by_portofolio_id($portofolio_id){
      $this->db->where("portofolio_id", $portofolio_id);
      $query = $this->db->get('portofolio_image');
      return $query->result();
    }

    function create_portofolio_image($data){
      $this->id             = $data['id'];
      $this->portofolio_id  = $data['portofolio_id'];
      $this->img_url        = $data['img_url'];
      $this->created_at     = date('Y-m-d H:i:s');

      $this->db->insert('portofolio_image', $this);
      return $this->db->affected_rows();
    }

    function update_portofolio_image($data, $id){
      $this->db->update('portofolio_image', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_portofolio_image($id){
      $this->db->where('id', $id);
      $this->db->delete('portofolio_image');
      return $this->db->affected_rows();
    }
  }
?>
