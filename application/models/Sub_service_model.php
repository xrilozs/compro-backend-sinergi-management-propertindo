<?php
  class Sub_service_model extends CI_Model{
    public $id;
    public $service_id;
    public $name;
    public $description;
    public $img_url;
    public $is_active;

    function get_sub_service($search=null, $service_id=null, $is_active=null, $order=null, $limit=null){
      $this->db->select("s2.*, s.name as service");
      $this->db->join("service s", "s.id = s2.service_id");
      if($search){
        $this->db->like("s2.name", $search);
        $this->db->or_like("s2.description", $search);
        $this->db->or_like("s.name", $search);
      }
      if($service_id){
        $this->db->where("s2.service_id", $service_id);
      }
      if(!is_null($is_active)){
        $this->db->where("s2.is_active", $is_active);
      }
      if($order){
        $this->db->order_by("s2.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('sub_service s2');
      return $query->result();
    }

    function count_sub_service($search=null,  $service_id=null, $is_active=null){
      $this->db->join("service s", "s.id = s2.service_id");
      if($search){
        $this->db->like("s2.name", $search);
        $this->db->or_like("s2.description", $search);
        $this->db->or_like("s.name", $search);
      }
      if($service_id){
        $this->db->where("s2.service_id", $service_id);
      }
      if(!is_null($is_active)){
        $this->db->where("s2.is_active", $is_active);
      }
      $this->db->from('sub_service s2');
      return $this->db->count_all_results();
    }
    
    function get_sub_service_by_service_id($service_id){
      $this->db->select("s2.*, s.name as service");
      $this->db->join("service s", "s.id = s2.service_id");
      $this->db->where("s2.is_active", 1);
      $query = $this->db->get('sub_service s2');
      return $query->result();
    }

    function get_sub_service_by_id($id, $is_assoc=false){
      $this->db->select("s2.*, s.name as service");
      $this->db->join("service s", "s.id = s2.service_id");
      $this->db->where("s2.id", $id);
      $query = $this->db->get('sub_service s2');
      if($is_assoc){
        return $query->num_rows() > 0 ? $query->row_array() : null;
      }else{
        return $query->num_rows() > 0 ? $query->row() : null;
      }
    }

    function create_sub_service($data){
      $this->id         = $data['id'];
      $this->service_id = $data['service_id'];
      $this->name       = $data['name'];
      $this->description= $data['description'];
      $this->img_url    = $data['img_url'];
      $this->is_active  = 1;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('sub_service', $this);
      return $this->db->affected_rows();
    }

    function update_sub_service($data, $id){
      $this->db->update('sub_service', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function inactive_sub_service($id){
      $data = array(
        "is_active" => 0
      );
      $this->db->where('id', $id);
      $this->db->update('sub_service', $data);
      return $this->db->affected_rows();
    }

    function active_sub_service($id){
      $data = array(
        "is_active" => 1
      );
      $this->db->where('id', $id);
      $this->db->update('sub_service', $data);
      return $this->db->affected_rows();
    }

    function delete_sub_service($id){
      $this->db->where('id', $id);
      $this->db->delete('sub_service');
      return $this->db->affected_rows();
    }
  }
?>
