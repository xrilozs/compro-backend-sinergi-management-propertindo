<?php
  class Message_model extends CI_Model{
    public $id;
    public $fullname;
    public $phone;
    public $email;
    public $company;
    public $content;
    public $has_source;
    public $product_id;
    public $service_id;
    public $is_read;

    function get_message($search=null, $is_read=null, $order=null, $limit=null){
      $this->db->select("m.*, p.name as product, s.name as service");
      $this->db->join("product p", "p.id = m.product_id", "LEFT");
      $this->db->join("service s", "s.id = m.service_id", "LEFT");
      if($search){
        $where_search = "CONCAT_WS(',', m.fullname, m.phone, m.email, m.company, m.content, p.name, s.name) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if(!is_null($is_read)){
        $this->db->where("is_read", $is_read);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("message m");
      $query = $this->db->get();
      return $query->result();
    }

    function count_message($search=null, $is_read=null){
      if($search){
        $where_search = "CONCAT_WS(',', fullname, phone, email, company, content) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if(!is_null($is_read)){
        $this->db->where("is_read", $is_read);
      }
      $this->db->from('message');
      return $this->db->count_all_results();
    }

    function get_message_by_id($id, $is_assoc=false){
      $this->db->select("m.*, p.name as product, s.name as service, s.description as service_description");
      $this->db->join("product p", "p.id = m.product_id", "LEFT");
      $this->db->join("service s", "s.id = m.service_id", "LEFT");
      $this->db->where("m.id", $id);
      $this->db->from("message m");
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() > 0 ? $query->row_array() : null;
      }else{
        return $query->num_rows() > 0 ? $query->row() : null;
      }
    }

    function create_message($data){
      $this->id         = $data['id'];
      $this->fullname   = $data['fullname'];
      $this->phone      = $data['phone'];
      $this->email      = $data['email'];
      $this->company    = $data['company'];
      $this->content    = $data['content'];
      $this->has_source = $data['has_source'];
      $this->product_id = array_key_exists("product_id", $data) ?  $data['product_id'] : null;
      $this->service_id = array_key_exists("service_id", $data) ?  $data['service_id'] : null;
      $this->is_read    = 0;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('message', $this);
      return $this->db->affected_rows();
    }

    function read_message($id){
      $data = array("is_read" => 1);
      $this->db->update('message', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function read_message_all(){
      $data = array("is_read" => 1);
      $this->db->update('message', $data, array('is_read' => 0));
      return $this->db->affected_rows();
    }
  }
?>
