<?php
  class Custom_page_model extends CI_Model{
    public $id;
    public $title;
    public $content;
    public $alias;
    public $position;
    public $is_active;

    function get_custom_page($search=null, $is_active=null, $order=null, $limit=null){
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      if($search){
        $where_search = "CONCAT_WS(',', title, content, alias, position) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('custom_page');
      return $query->result();
    }

    function count_custom_page($search=null, $is_active=null){
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      if($search){
        $where_search = "CONCAT_WS(',', title, content, alias, position) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      $this->db->from('custom_page');
      return $this->db->count_all_results();
    }
    
    function get_custom_page_by_position($position){
      $this->db->where_in("position", $position);
      $this->db->where("is_active", 1);
      $query = $this->db->get('custom_page');
      return $query->result();
    }

    function get_custom_page_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('custom_page');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }
    
    function get_custom_page_by_alias($alias){
      $this->db->where("alias", $alias);
      $query = $this->db->get('custom_page');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_custom_page($data){
      $this->id         = $data['id'];
      $this->title      = $data['title'];
      $this->content    = $data['content'];
      $this->alias      = $data['alias'];
      $this->position   = $data['position'];
      $this->is_active  = 1;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('custom_page', $this);
      return $this->db->affected_rows();
    }

    function update_custom_page($data, $id){
      $this->db->update('custom_page', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function inactive_custom_page($id){
      $data = array(
        "is_active" => 0
      );
      $this->db->update('custom_page', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active_custom_page($id){
      $data = array(
        "is_active" => 1
      );
      $this->db->update('custom_page', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_custom_page($id){
      $this->db->where('id', $id);
      $this->db->delete('custom_page');
      return $this->db->affected_rows();
    }
  }
?>
