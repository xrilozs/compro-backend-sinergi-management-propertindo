<?php
  class Admin_model extends CI_Model {
    public $id;
    public $username;
    public $password;
    public $fullname;
    public $is_active;

    function get_admin($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', username, fullname) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      $this->db->from('admin');
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_admin($search=null){
      $this->db->from('admin');
      if($search){
        $where_search = "CONCAT_WS(',', username, fullname) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      return $this->db->count_all_results();
    }

    function get_admin_by_username($username, $is_active=null, $is_assoc=false){
      $this->db->where("username", $username);
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $query = $this->db->get('admin');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_admin_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from('admin');
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_admin($data){
      $this->id         = $data['id'];
      $this->username   = $data['username'];
      $this->password   = $data['password'];
      $this->fullname   = $data['fullname'];
      $this->is_active  = 1;
      $this->created_at = date("Y-m-d H:i:s");
      $this->db->insert('admin', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_admin($data, $id){
      $this->db->update('admin', $data, array("id" => $id));
      return $this->db->affected_rows();
    }

    function inactive_admin($id){
      $data = array(
        'is_active' => 0,
      );
      $this->db->update('admin', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active_admin($id){
      $data = array(
        'is_active' => 1
      );
      $this->db->update('admin', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
    
    function change_password_admin($password, $id){
      $data = array(
        'password' => $password
      );
      $this->db->update('admin', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
