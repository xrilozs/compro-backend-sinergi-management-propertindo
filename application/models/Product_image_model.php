<?php
  class Product_image_model extends CI_Model{
    public $id;
    public $product_id;
    public $img_url;

    function get_product_image_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('product_image');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_product_image_by_product_id($product_id){
      $this->db->where("product_id", $product_id);
      $query = $this->db->get('product_image');
      return $query->result();
    }

    function create_product_image($data){
      $this->id         = $data['id'];
      $this->product_id = $data['product_id'];
      $this->img_url    = $data['img_url'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('product_image', $this);
      return $this->db->affected_rows();
    }

    function update_product_image($data, $id){
      $this->db->update('product_image', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_product_image($id){
      $this->db->where('id', $id);
      $this->db->delete('product_image');
      return $this->db->affected_rows();
    }
  }
?>
