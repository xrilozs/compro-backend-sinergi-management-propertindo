<?php
  class Faq_model extends CI_Model{
    public $id;
    public $question;
    public $answer;

    function get_faqs($search=null, $order=null, $limit=null){
      if($search){
        $this->db->like("question", $search);
        $this->db->or_like("answer", $search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('faq');
      return $query->result();
    }

    function count_faq($search=null){
      if($search){
        $this->db->like("question", $search);
        $this->db->or_like("answer", $search);
      }
      $this->db->from('faq');
      return $this->db->count_all_results();
    }

    function get_faq_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('faq');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_faq($data){
      $this->id         = $data['id'];
      $this->question   = $data['question'];
      $this->answer     = $data['answer'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('faq', $this);
      return $this->db->affected_rows();
    }

    function update_faq($data, $id){
      $this->db->update('faq', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_faq($id){
      $this->db->where('id', $id);
      $this->db->delete('faq');
      return $this->db->affected_rows();
    }
  }
?>
