<?php
  class Product_category_model extends CI_Model{
    public $id;
    public $name;
    public $alias;
    public $img_url;
    public $is_active;

    function get_product_category($search=null, $is_active=null, $order=null, $limit=null){
      if($search){
        $this->db->like("name", $search);
        $this->db->or_like("alias", $search);
      }
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('product_category');
      return $query->result();
    }

    function count_product_category($search=null, $is_active=null){
      if($search){
        $this->db->like("name", $search);
        $this->db->or_like("alias", $search);
      }
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $this->db->from('product_category');
      return $this->db->count_all_results();
    }
    
    function get_product_category_all(){
      $this->db->where("is_active", 1);
      $query = $this->db->get('product_category');
      return $query->result();
    }

    function get_product_category_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('product_category');
      if($is_assoc){
        return $query->num_rows() > 0 ? $query->row_array() : null;
      }else{
        return $query->num_rows() > 0 ? $query->row() : null;
      }
    }
    
    function get_product_category_by_alias($alias){
      $this->db->where("alias", $alias);
      $query = $this->db->get('product_category');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_product_category($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->alias      = $data['alias'];
      $this->img_url    = $data['img_url'];
      $this->is_active  = 1;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('product_category', $this);
      return $this->db->affected_rows();
    }

    function update_product_category($data, $id){
      $this->db->update('product_category', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function inactive_product_category($id){
      $data = array(
        "is_active" => 0
      );
      $this->db->where('id', $id);
      $this->db->update('product_category', $data);
      return $this->db->affected_rows();
    }

    function active_product_category($id){
      $data = array(
        "is_active" => 1
      );
      $this->db->where('id', $id);
      $this->db->update('product_category', $data);
      return $this->db->affected_rows();
    }
  }
?>
