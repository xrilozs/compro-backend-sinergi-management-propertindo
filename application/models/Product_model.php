<?php
  class Product_model extends CI_Model{
    public $id;
    public $category_id;
    public $name;
    public $alias;
    public $description;
    public $price;
    public $is_active;

    function get_product($search=null, $is_active=null, $order=null, $limit=null){
      $this->db->select("p.*, c.name as category");
      $this->db->join("product_category c", "c.id = p.category_id");
      if($search){
        $where_search = "CONCAT_WS(',', p.name, p.alias, p.description, p.price, c.name) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $this->db->where("p.is_active", $is_active);
      }
      if($order){
        $this->db->order_by("p.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('product p');
      return $query->result();
    }

    function count_product($search=null, $is_active=null){
      $this->db->join("product_category c", "c.id = p.category_id");
      if($search){
        $where_search = "CONCAT_WS(',', p.name, p.alias, p.description, p.price, c.name) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $this->db->where("p.is_active", $is_active);
      }
      $this->db->from('product p');
      return $this->db->count_all_results();
    }

    function get_product_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('product');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_product_by_alias($alias, $is_assoc=false){
      $this->db->where("alias", $alias);
      $this->db->where("is_active", 1);
      $query = $this->db->get('product');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_product_by_category_id($category_id, $is_active=1){
      $this->db->where("category_id", $category_id);
      $this->db->where("is_active", $is_active);
      $query = $this->db->get('product');
      return $query->result();
    }

    function create_product($data){
      $this->id           = $data['id'];
      $this->category_id  = $data['category_id'];
      $this->name         = $data['name'];
      $this->alias        = $data['alias'];
      $this->description  = $data['description'];
      // $this->price        = $data['price'];
      $this->is_active    = 1;
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('product', $this);
      return $this->db->affected_rows();
    }

    function update_product($data, $id){
      $this->db->update('product', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active_product($id){
      $this->db->update('product', array("is_active"=>1), array('id' => $id));
      return $this->db->affected_rows();
    }

    function inactive_product($id){
      $this->db->update('product', array("is_active"=>0), array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_product($id){
      $this->db->where('id', $id);
      $this->db->delete('product');
      return $this->db->affected_rows();
    }
  }
?>
